<?php
namespace app\assets;


use yii\web\AssetBundle;

class StarRatingAsset extends AssetBundle
{
    public $sourcePath = '@bower/bootstrap-star-rating';
    public $css = [
        'css/star-rating.css',
        'themes/krajee-fa/theme.css'
    ];
    public $js = [
        'js/star-rating.js',
        'themes/krajee-fa/theme.js'
    ];
    public $depends = [
        'yii\bootstrap\BootstrapAsset',
    ];

}
