<?php
use kartik\datecontrol\Module;

return [
    'adminEmail' => 'admin@example.com',
    'languages' => ['en-US','pt','de','fr','es','zh-CN','ar-ae'],
    'bootstrap_theme' => 'theme1',
    'bootstrap_themes' => ['Challenge8'=>'theme5','Amelia'=>'theme1','Slate'=>'theme2','Lumen'=>'theme3','Super Hero'=>'theme4'],
    // format settings for displaying each date attribute (ICU format example)
    'dateControlDisplay' => [
        Module::FORMAT_DATE => 'yyyy-MM-dd',
        Module::FORMAT_TIME => 'hh:mm:ss a',
        Module::FORMAT_DATETIME => 'yyyy-MM-dd hh:mm:ss a',
    ],
    // format settings for saving each date attribute (PHP format example)
    'dateControlSave' => [
        Module::FORMAT_DATE => 'php:U', // saves as unix timestamp
        Module::FORMAT_TIME => 'php:H:i:s',
        Module::FORMAT_DATETIME => 'php:Y-m-d H:i:s',
    ]
];
