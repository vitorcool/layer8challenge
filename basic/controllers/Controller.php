<?php
/**
 * Created by PhpStorm.
 * User: Vitor Oliveira
 * Date: 9/7/2017
 * Time: 6:48 PM
 */

namespace app\controllers;

use Yii;
use yii\helpers\Url;

class Controller extends \yii\web\Controller
{
    public function beforeAction($action)
    {
        $theme= Yii::$app->request->get('theme');
        if(!empty($theme)) {
            if($this->setTheme($theme))
                $this->redirect( Url::current(['theme'=>null]));
        }

        $lang= Yii::$app->request->get('lang');
        if(!empty($lang)) {
            if($this->setLanguage($lang))
                $this->redirect( Url::current(['lang'=>null]));;
        }


        // get the cookie collection (yii\web\CookieCollection) from the "request" component
        $cookies = \Yii::$app->request->cookies;

        // you may also use $cookies like an array
        if (isset($cookies['language'])) {
            \Yii::$app->language = $cookies['language']->value;
        }

        // you may also use $cookies like an array
        if (isset($cookies['theme'])) {
            \Yii::$app->params['bootstrap_theme'] = $cookies['theme']->value;
        }


        return parent::beforeAction($action);
    }

    public function setLanguage($language){
        if(! in_array($language, Yii::$app->params['languages'])){
            return false;
        }
        // get the cookie collection (yii\web\CookieCollection) from the "response" component
        $cookies = Yii::$app->response->cookies;

        // add a new cookie to the response to be sent
        $cookies->add(new \yii\web\Cookie([
            'name' => 'language',
            'value' => $language,
        ]));
        return true;
    }

    private function setTheme($bootstrap_theme){
        if(! in_array($bootstrap_theme, Yii::$app->params['bootstrap_themes'])){
            return false;
        }
        // get the cookie collection (yii\web\CookieCollection) from the "response" component
        $cookies = Yii::$app->response->cookies;

        // add a new cookie to the response to be sent
        $cookies->add(new \yii\web\Cookie([
            'name' => 'theme',
            'value' => $bootstrap_theme,
        ]));
        return true;
    }
}