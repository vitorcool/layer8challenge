<?php

namespace app\controllers;

use app\components\MyHelper;
use Yii;
use app\models\User;
use app\models\UserSearch;
use yii\captcha\Captcha;
use yii\captcha\CaptchaAction;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                //'only' => ['login', 'logout', 'signup'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['register','validate-register-user'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['logout'],
                        'roles' => ['@'],
                    ],
                    [
                      //  'actions' => ['special-callback'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            $u=Yii::$app->getUser();
                            if($u->getIsGuest())
                                return false;
                            else {
                                return User::isAdmin() === true;
                            }
                        }
                    ],

                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $_id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }


    public function actionValidateRegisterUser()
    {
        $model = new User();
        $model->load(Yii::$app->request->post());
        $ret= \yii\widgets\ActiveForm::validate($model);
        $ret= Json::encode($ret);
        return $ret;
    }
    /**
     * validate user rules
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionValidateUser()
    {
        $model = new User();

        if(!Yii::$app->request->isAjax) {
            return "";
        }
        $model->load(Yii::$app->request->post());
        $ret= \yii\widgets\ActiveForm::validate($model);
        $ret= Json::encode($ret);
        return $ret;
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new User();
        $model->state =0;

        $ajax = Yii::$app->request->get('ajax');
        if(Yii::$app->request->isAjax || $ajax==true){
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return [
                    'result'=>true,
                    'model'=>$model->toArray()
                ];
            } else {
                return $this->renderAjax('create', [
                    'model' => $model,
                ]);
            }
        }else {
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => (string)$model->_id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionRegister()
    {
        $model = new User();
        $model->scenario = 'register'; // extra set of rules ( or just one )

        $model->state =0;
        $ajax = Yii::$app->request->get('ajax');
        if(Yii::$app->request->isAjax || $ajax==true){
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return [
                    'result'=>true,
                    'model'=>$model->toArray()
                ];
            } else {
                return $this->renderAjax('register', [
                    'model' => $model,
                ]);
            }
        }else {
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['/site/login']);
            } else {
                return $this->render('register', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $_id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->state =0;

        $ajax = Yii::$app->request->get('ajax');
        if(Yii::$app->request->isAjax || $ajax==true){
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return [
                    'result'=>true,
                    'model'=>$model->toArray()
                ];
            } else {
                return $this->renderAjax('update', [
                    'model' => $model,
                ]);
            }
        }else {
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => (string)$model->_id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $_id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $_id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
