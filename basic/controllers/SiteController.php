<?php

namespace app\controllers;

use app\models\ES\Restaurant;
use app\models\RestaurantComments;
use app\models\SiteContact;
use MongoDB\BSON\UTCDateTime;
use phpDocumentor\Reflection\DocBlock\Tags\Throws;
use Yii;
use yii\captcha\CaptchaAction;
use yii\data\ActiveDataProvider;
use yii\db\BaseActiveRecord;
use yii\db\Exception;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\helpers\Json;
use yii\helpers\Url;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                   // 'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => CaptchaAction::className(),
                //'testLimit'=>-2,
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }


    public function actionMove(){
        $pageSize = 100;
        $page=Yii::$app->request->get('page');
        $page=empty($page)?0:$page;
        $echoModel= function( $model,$i ) use(&$echoModel){
            echo '[';
            foreach ($model as $recno=>$record) {
                echo  '<br>'.str_repeat('&nbsp;',$i*4)  . $recno . ':';

                $isArray=$record instanceof \ArrayObject;
                $felem=$isArray ? reset($record):null;
                $feIsBAR=$felem instanceof  BaseActiveRecord;

                if($isArray && $feIsBAR){
                    foreach ($record as $idx=>$recno)
                        $echoModel( $record,$i+1 );
                }
                else if($record instanceof BaseActiveRecord){
                    $echoModel( $record,$i+1 );
                }else{
                    echo Json::encode($record);
                }
                echo ',';
            }
            echo '}';
            //return $ret;
        };

        do {
            $page++;
            $source = \app\models\Restaurant::find();
            $dataProvider = new ActiveDataProvider([
                'query' => $source,
                'pagination' => [
                    'pageSize' => $pageSize,
                ],
            ]);

            $dataProvider->getPagination()->page=$page;



            $model=null;

            ob_start();
            try {
                echo '<br>{{ ' . $page . ' }}';
                $i=1;
                $models = $dataProvider->getModels();
                foreach ($models as $model) {
                    //echo $echoModel($model,0);

                    $data = $model->getAttributes();
                    unset($data['_id']);

                    $target = new \app\models\ES\Restaurant();


                    echo '<br>('.  (($page - 1) * $pageSize + $i) . ')';
                    if (!($target->load(['Restaurant' => $data]) && $target->validate())) {
                        var_dump($target->getErrors());
                    }else{
                        echo 'restaurant_id:' . $target->restaurant_id;
                        echo '<br>';
                        foreach($target->grades as $ii=>$grade) {
                            if(isset($grade->date)) {
                                $a = $grade->date->toDateTime()->format('Y-m-d\TH:i:s\Z');
                                $grade->date = $a;
                            }
                        }
                        foreach($target->comments as $ii=>$comment) {
                            if(isset($comment->date)) {
                                $a =  $comment->date->toDateTime()->format('Y-m-d\TH:i:s\Z');
                                $comment->date =$a;
                            }
                        }
                        $target->save(false);

                        $i++;
                    }

                }
            }catch (Exception $e) {
                throw $e;
                //echo "<br><br>##### error ##### ". $e->getMessage().', '.$e->getFile().' ( '.$this->$e->getLine()." ) <br>";
            }finally {
                ob_end_flush();
                ob_flush();
                flush();
                unset($source);
                unset($dataProvider);
                unset($target);
            }
        }while(false);
        if(!empty($models)) {
            $url =  Url::to(['site/move', 'page'=>$page], true);
            $js= <<<JS
                <script>
                    document.location.href = "$url";
                </script>         
JS;
        echo $js;
        }else {
            echo "{{ fim }}";
        }

    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }


    public function actionValidateContact(){
        if(!Yii::$app->request->isAjax) return "";

        $model = new SiteContact();

        $model->load(Yii::$app->request->post());
        $ret= \yii\widgets\ActiveForm::validate($model);
        $ret= Json::encode($ret);
        return $ret;
    }
    /**
     * Displays contact page.
     *
     * @return string
     */
    public function actionContact()
    {
        $model = new SiteContact();

        if(! Yii::$app->user->isGuest){
            $u = Yii::$app->user->getIdentity(false);
            $model->name  = $u->username;
            $model->email = $u->email;
        }else
            $model->scenario = SiteContact::SCENARIO_USER_IS_GUEST;

        $ajax= Yii::$app->request->get('ajax');
        if (Yii::$app->request->isAjax || $ajax=='true') {
            if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->contact(Yii::$app->params['adminEmail'])) {
                $model->save(false);
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return [
                    'result'=>true,
                    'model'=>$model->toArray()
                ];
            }
            return $this->renderAjax('contact', [
                'model' => $model,
            ]);
        }else {
            if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->contact(Yii::$app->params['adminEmail'])) {
                $model->save(false);
                Yii::$app->session->setFlash('contactFormSubmitted');
                return $this->refresh();
            }
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
