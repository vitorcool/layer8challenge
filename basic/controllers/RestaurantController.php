<?php

namespace app\controllers;

use app\components\MyHelper;
use app\models\RestaurantComments;
use app\models\RestaurantGrades;
use app\models\User;
use MongoDB\BSON\UTCDateTime;
use Yii;
use app\models\Restaurant;
use app\models\RestaurantSearch;
use app\models\RestaurantSearch2;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RestaurantController implements the CRUD actions for Restaurant model.
 */
class RestaurantController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                //'only' => ['login', 'logout', 'signup'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index2','create-comment','grades-stat1','map',
                                    'comments-stat1','comments-stat2','comment-details2','grade-details2','validate-comment'],
                        'roles' => ['?'],
                    ],
                    [
                        //  'actions' => ['special-callback'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            $u=Yii::$app->getUser();
                            if($u->getIsGuest())

                                return false;
                            elseif( User::isAdmin() === true){
                                return true;
                            }else{//user
                                return in_array($action->id,['index2','index','grades-stat1','map',
                                        'comments-stat1','comments-stat2','embeb-details','comment-details','comment-details2',
                                        'grade-details','grade-details2','validate-comment','validate-grade','update-comment','create-comment'
                                    ]
                                );
                            }
                        }
                    ],

                ],
            ],
        ];
    }

    /**
     * Lists all Restaurant models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RestaurantSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionGradesStat1(){
        $data = Restaurant::countGradesPerMonth();

        $ajax= Yii::$app->request->get('ajax');
        if (Yii::$app->request->isAjax || $ajax=='true') {
            return $this->renderAjax('grades-stat1', [
                'data' => $data,
            ]);
        }else {
            return $this->render('grades-stat1', [
                'data' => $data,
            ]);
        }
    }

    public function actionCommentsStat1(){
        $data = Restaurant::countCommentsPerDay();

        $ajax= Yii::$app->request->get('ajax');
        if (Yii::$app->request->isAjax || $ajax=='true') {
            return $this->renderAjax('comments-stat1', [
                'data' => $data,
            ]);
        }else {
            return $this->render('comments-stat1', [
                'data' => $data,
            ]);
        }
    }

    public function actionCommentsStat2()
    {
        $data = Restaurant::timeBetweenComments();

        $ajax = Yii::$app->request->get('ajax');
        if (Yii::$app->request->isAjax || $ajax == 'true') {
            return $this->renderAjax('comments-stat2', [
                'data' => $data,
            ]);
        } else {
            return $this->render('comments-stat2', [
                'data' => $data,
            ]);
        }
    }

    /**
     * Lists all Restaurant models.
     * @return mixed
     */
    public function actionIndex2()
    {
        $searchModel = new RestaurantSearch2();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $b = $this->view->registerAssetBundle(\dosamigos\google\maps\MapAsset::className() );
        $b->options=[
            'key' => 'AIzaSyCJcLn_nkGGMnUtvr9S8cISsmz5C3CfXuY',
            'language' => Yii::$app->language,
            'version' => '3.1.18'
        ];
        $b->init();
        return $this->render('index2', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Restaurant models.
     * @return mixed
     */
    public function actionMap($id)
    {
        $b = $this->view->registerAssetBundle(\dosamigos\google\maps\MapAsset::className() );
        $b->options=[
            'key' => 'AIzaSyCJcLn_nkGGMnUtvr9S8cISsmz5C3CfXuY',
            'language' => Yii::$app->language,
            'version' => '3.1.18'
        ];
        $b->init();

        $ajax= Yii::$app->request->get('ajax');
        if (Yii::$app->request->isAjax || $ajax=='true') {
            return $this->renderAjax('_map', [
                'model' => $this->findModel($id),
            ]);
        }else {
            return $this->render('_map', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    public function actionTest()
    {
        $model = Restaurant::find()->where(['!=','_id',""])->limit(1)->one();
        //$model->refreshFromEmbedded();

        $a=$model->address;
        foreach($a as $k=>$v) {
            echo "<br>{$k}={$v}";
        }

        return;

        $g = $model->grades;
        foreach($g as $i=>$vv) {
            echo "<br>grade idx({$i})";
            foreach($vv as $k=>$v) {
                if($v instanceof UTCDateTime)
                    $v= $v->toDateTime()->format("Y-m-d ");
                echo "<br>    {$k}={$v}";
            }
        }



    }

    /**
     * Displays a single Restaurant model.
     * @param integer $_id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }


    public function actionValidateRestaurant(){
        if(!Yii::$app->request->isAjax) return "";

        $model = new Restaurant();
        $model->load(Yii::$app->request->post());
        $addr=$model->address;
        $addr->load(Yii::$app->request->post());
        $ret= \yii\widgets\ActiveForm::validate($model);
        $ret= Json::encode($ret);
        return $ret;
    }

    /**
     * Creates a new Restaurant model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Restaurant();
        $model->scenario="create";
        $modelAddress = $model->address;

        $ajax= Yii::$app->request->get('ajax');
        if (Yii::$app->request->isAjax || $ajax=='true') {
            if( $model->load(Yii::$app->request->post()) && $modelAddress->load(Yii::$app->request->post()) &&
                $model->validate()
            ) {
                $model->save();
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return [
                        'result'=>true,
                        'model'=>$model->toArray()
                    ];
            } else {
                return $this->renderAjax('create', [
                    'model' => $model,
                ]);
            }
        }else {
            if( $model->load(Yii::$app->request->post()) && $modelAddress->load(Yii::$app->request->post()) &&
                $model->validate()
            ) {
                $model->save();
                return $this->redirect(['view', 'id' => (string)$model->_id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }

    }

    /**
     * Updates an existing Restaurant model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $_id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->scenario = 'update';

        $post = Yii::$app->request->post();
        $modelAddress = $model->address;
        $ajax= Yii::$app->request->get('ajax');
        if (Yii::$app->request->isAjax || $ajax=='true') {
            if ($model->load($post) && $modelAddress->load($post) &&
                $model->validate()
            ) {
                $model->save();
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return [
                    'result'=>true,
                    'model'=>$model->toArray()
                ];
            } else {
                return $this->renderAjax('update', [
                    'model' => $model,
                ]);
            }
        }else {
            if ($model->load($post) && $modelAddress->load($post) &&
                $model->validate()
            ) {
                $model->save();
                return $this->redirect(['view', 'id' => (string)$model->_id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionEmbedDetails(){
        $id = Yii::$app->request->post('expandRowKey');
        if(empty($id))
            $id=Yii::$app->request->get('id');
        $model = $this->findModel($id);

        $model = $this->findModel($id);
        $modelComments=$model->comments;

        $ajax = Yii::$app->request->get('ajax');
        if (Yii::$app->request->isAjax || $ajax=='true') {
            return $this->renderAjax('EmbedDetails', [
                'id' => $id,
                'model'  => $model,
            ]);
        } else {
            return $this->render('EmbedDetails', [
                'id' => $id,
                'model'  => $model,
            ]);
        }
    }

    public function actionCommentDetails(){
        $id = Yii::$app->request->post('expandRowKey');
        if(empty($id))
            $id=Yii::$app->request->get('id');
        $model = $this->findModel($id);

        $model = $this->findModel($id);
        $modelComments=$model->comments;

        $ajax = Yii::$app->request->get('ajax');
        if (Yii::$app->request->isAjax || $ajax=='true') {
            return $this->renderAjax('commentDetails', [
                'id' => $id,
                'model'  => $model,
                'comments' => $modelComments,
            ]);
        } else {
            return $this->render('commentDetails', [
                'id' => $id,
                'model'  => $model,
                'comments' => $modelComments,
            ]);
        }
    }

    public function actionCommentDetails2(){
        $id = Yii::$app->request->post('expandRowKey');
        if(empty($id))
            $id=Yii::$app->request->get('id');
        $model = $this->findModel($id);

        $model = $this->findModel($id);
        $modelComments=$model->comments;

        $ajax = Yii::$app->request->get('ajax');
        if (Yii::$app->request->isAjax || $ajax=='true') {
            return $this->renderAjax('commentDetails2', [
                'id' => $id,
                'model'  => $model,
                'comments' => $modelComments,
            ]);
        } else {
            return $this->render('commentDetails2', [
                'id' => $id,
                'model'  => $model,
                'comments' => $modelComments,
            ]);
        }
    }


    public function actionGradeDetails(){
        $id = Yii::$app->request->post('expandRowKey');
        if(empty($id))
            $id=Yii::$app->request->get('id');
        $ajax = Yii::$app->request->get('ajax');

        $model = $this->findModel($id);
        $modelGrade=$model->grades;

        if (Yii::$app->request->isAjax || $ajax=='true') {
            return $this->renderAjax('gradeDetails', [
                    'id' => $id,
                    'model'  => $model,
                    'grades' => $modelGrade,
                ]);
        } else {
            return $this->render('gradeDetails', [
                'id' => $id,
                'model'  => $model,
                'grades' => $modelGrade,
            ]);
        }
    }

    public function actionGradeDetails2(){
        $id = Yii::$app->request->post('expandRowKey');
        if(empty($id))
            $id=Yii::$app->request->get('id');
        $model = $this->findModel($id);

        $model = $this->findModel($id);
        $modelGrades=$model->grades;

        $ajax = Yii::$app->request->get('ajax');
        if (Yii::$app->request->isAjax || $ajax=='true') {
            return $this->renderAjax('gradeDetails2', [
                'id' => $id,
                'model'  => $model,
                'grades' => $modelGrades,
            ]);
        } else {
            return $this->render('gradeDetails2', [
                'id' => $id,
                'model'  => $model,
                'grades' => $modelGrades,
            ]);
        }
    }

    public function actionValidateComment(){
        if(!Yii::$app->request->isAjax) return "";

        $model = new RestaurantComments();
        if(!Yii::$app->user->isGuest){
            $modelComment->user_id = Yii::$app->user->getId();
        }
        $model->load(Yii::$app->request->post());
        $ret= \yii\widgets\ActiveForm::validate($model);
        $ret= Json::encode($ret);
        return $ret;
    }

    public function actionValidateGrade(){
        if(!Yii::$app->request->isAjax) return "";

        $model = new RestaurantGrades();
        $model->load(Yii::$app->request->post());
        $ret= \yii\widgets\ActiveForm::validate($model);
        $ret= Json::encode($ret);
        return $ret;
    }

    public function actionUpdateGrade()
    {
        $id  = Yii::$app->request->get('rest_id');
        $idx = Yii::$app->request->get('idx');


        $model = $this->findModel($id);
        $model->scenario = 'update';

        $modelGrade = $model->grades[$idx];

        $ajax= Yii::$app->request->get('ajax');
        if (Yii::$app->request->isAjax || $ajax=='true') {
            if ($modelGrade->load(Yii::$app->request->post()) && $model->validate()) {
                $model->save();
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return [
                    'result'=>true,
                    'model'=>$model->toArray()
                ];
            } else
                return $this->renderAjax('updateGrade', [
                    'id' => $id,
                    'idx' => $idx,
                    'model' => $modelGrade,
                ]);
        } else {
            if ($modelGrade->load(Yii::$app->request->post()) && $model->validate()) {
                $model->save();
                return $this->redirect(['view', 'id' => (string)$model->_id]);
            } else
                return $this->render('updateGrade', [
                    'id'    => $id,
                    'idx'   => $idx,
                    'model' => $modelGrade,
                ]);
        }
    }

    public function actionUpdateComment()
    {
        $id  = Yii::$app->request->get('rest_id');
        $idx = Yii::$app->request->get('idx');

        $model = $this->findModel($id);

        $model->scenario = 'update';

        $modelComment = $model->comments[$idx];
      //  $modelComment->scenario= RestaurantComments::SCENARIO_USER_IS_LOGGED_IN;

        if(!Yii::$app->user->isGuest && (! User::isAdmin())){
            $authId = Yii::$app->user->getId();
            $commentUserId=MyHelper::fix_id($modelComment->user_id);
            if($commentUserId!==$authId){
                throw new NotFoundHttpException(Yii::t('restaurant',"Can't update this comment."));
            }
        }

        $ajax= Yii::$app->request->get('ajax');
        if (Yii::$app->request->isAjax || $ajax=='true') {
            if ($modelComment->load(Yii::$app->request->post()) && $model->validate()) {
                $model->save();
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return [
                    'result'=>true,
                    'model'=>$model->toArray()
                ];
            } else
                return $this->renderAjax('updateComment', [
                    'id' => $id,
                    'idx' => $idx,
                    'model' => $modelComment,
                ]);
        } else {
            if ($modelComment->load(Yii::$app->request->post()) && $model->validate()) {
                $model->save();
                return $this->redirect(['view', 'id' => (string)$model->_id]);
            } else
                return $this->render('updateComment', [
                    'id'    => $id,
                    'idx'   => $idx,
                    'model' => $modelComment,
                ]);
        }
    }

    public function actionCreateGrade()
    {
        $id  = Yii::$app->request->get('rest_id');
        $model = $this->findModel($id);

        $modelGrade = new RestaurantGrades();
        $model->grades[] = $modelGrade;
        $ajax= Yii::$app->request->get('ajax');
        if (Yii::$app->request->isAjax || $ajax=='true') {
            if ($modelGrade->load(Yii::$app->request->post()) && $modelGrade->validate() && $model->validate()){
                $model->save();
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return [
                    'result'=>true,
                    'model'=>$model->toArray()
                ];
            } else
                return $this->renderAjax('createGrade', [
                    'id' => $id,
                    'model' => $modelGrade,
                ]);
        } else {
            if ($modelGrade->load(Yii::$app->request->post()) && $modelGrade->validate() && $model->validate()){
                $model->save();
                return $this->redirect(['view', 'id' => (string)$model->_id]);
            } else
                return $this->render('createGrade', [
                    'id'    => $id,
                    'model' => $modelGrade,
                ]);
        }
    }

    public function actionCreateComment()
    {
        $id  = Yii::$app->request->get('rest_id');
        $model = $this->findModel($id);

        $modelComment = new RestaurantComments();
        if(!Yii::$app->user->isGuest){
            $modelComment->user_id = Yii::$app->user->getId();
        }

        $model->comments[] = $modelComment;

        $ajax= Yii::$app->request->get('ajax');
        if (Yii::$app->request->isAjax || $ajax=='true') {
            if ($modelComment->load(Yii::$app->request->post()) && $modelComment->validate() && $model->validate()){
                $model->save();
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return [
                    'result'=>true,
                    'model'=>$model->toArray()
                ];
            } else
                return $this->renderAjax('createComment', [
                    'id' => $id,
                    'model' => $modelComment,
                ]);
        } else {
            if ($modelComment->load(Yii::$app->request->post()) && $modelComment->validate() && $model->validate()){
                $model->save();
                return $this->redirect(['view', 'id' => (string)$model->_id]);
            } else
                return $this->render('createComment', [
                    'id'    => $id,
                    'model' => $modelComment,
                ]);
        }
    }

    /**
     * Deletes an existing Restaurant model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $_id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Restaurant model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $_id
     * @return Restaurant the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Restaurant::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
