<?php
/**
 * default css for funcyEmail
.email-funcy-name{
    border-bottom: 1px solid #ecd1d1 !important;
    padding: 0px 3px 0px 2px;
    display: inline-block;
}
.email-funcy-at {
    font-size: large;
    font-weight:bold;
    position:relative;
    top: 3px;
    left: -1px;
    display: inline-block;
    border-radius: 30px 400% 30px 20px;
    color:#d41414;
    border: 1px solid white;
    background-color:white;
}
.email-funcy-domain {
    font-size: xx-small;
    font-weight:bold;
    position:relative;
    left: -4px;
    top: 4px;
    color:#d41414;
}
 */
namespace app\components;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;


class EmailColumn extends \yii\grid\DataColumn
{

    public $format = 'html';

    public $emailNameOptions = ['class'=>'email-funcy-name','data-xx'=>'fdsfsdfdsf','style'=>'display: inline-block;'];
    public $emailAtDomainOptions = ['class'=>'email-funcy-atDomain','style'=>'display: inline-block;'];
    public $emailAtOptions = ['class'=>'email-funcy-at'];
    public $emailDomainOptions = ['class'=>'email-funcy-domain'];


    /**
     * @param $email
     * @return string
     */
    public function renderEmail($email){

        $arr=explode('@', $email );

        if(empty($email) || (($arr) && sizeof($arr)<=1)) return $email;

        $emailName      = Html::tag('span', $this->grid->formatter->format($arr[0], $this->format), $this->emailNameOptions);
        $emailDomain    = Html::tag('span', $this->grid->formatter->format($arr[1], $this->format), $this->emailDomainOptions);
        $emailAt        = Html::tag('span', '@' , $this->emailAtOptions );
        $emailAtDomain  = Html::tag('span', $emailAt . $emailDomain , $this->emailAtDomainOptions );

        return $emailName . $emailAtDomain;
    }

    /**
     * @param mixed $model
     * @param mixed $key
     * @param int $index
     * @return mixed|null|string
     *
     */
    public function getDataCellValue($model, $key, $index)
    {
        if ($this->value !== null) {
            if (is_string($this->value)) {
                return $this->renderEmail(ArrayHelper::getValue($model, $this->value));
            } else {
                return call_user_func($this->value, $model, $key, $index, $this);
            }
        } elseif ($this->attribute !== null) {
            return $this->renderEmail(ArrayHelper::getValue($model, $this->attribute));
        }
        return null;
    }

    /**
     * @inheritdoc
     */
    protected function renderDataCellContent($model, $key, $index)
    {
        if ($this->content === null) {
            return $this->grid->formatter->format($this->getDataCellValue($model, $key, $index), 'html');
        } else {
            return parent::renderDataCellContent($model, $key, $index);
        }
    }
}
