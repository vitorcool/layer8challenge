<?php

namespace app\components;


use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class DetailView extends \yii\widgets\DetailView
{
    public $groupTemplate = '{group}';
    public $template = '<div class="fsi-row-md-level fsi-row-lg-level">{label}{value}</div>';
    public $labelOptions = [
        'tag' => 'div',
        'class'=>"col-xs-12 col-sm-12 col-md-3 col-lg-3",
    ];
    public $valueOptions = [
        'tag'=> 'div',
        'class'=>"col-xs-12 col-sm-12 col-md-9 col-lg-9",
    ];

    /**
     * Renders the detail view.
     * This is the main entry of the whole detail view rendering.
     */
    public function run()
    {
        $rows = [];
        $i = 0;
        foreach ($this->attributes as $attribute) {
            $rows[] = $this->renderAttribute($attribute, $i++);
        }

        $class = 'div-detail-view';
        $options = $this->options;
        $options['class']= (isset($options['class'])) ? $options['class'].' '.$class : $class;
        $tag = ArrayHelper::remove($options, 'tag', 'div');
        $ret =Html::tag($tag, implode("\n", $rows), $options);
        return $this->renderGroupTemplate($ret);
    }

    /**
     * Renders a single attribute.
     * @param string html code
     * @return string the rendering result
     */
    protected function renderGroupTemplate($group)
    {
        if (is_string($this->groupTemplate)) {
            return strtr($this->groupTemplate, [
                '{group}' => $group,
            ]);
        }
    }

    /**
     * Renders a single attribute.
     * @param array $attribute the specification of the attribute to be rendered.
     * @param integer $index the zero-based index of the attribute in the [[attributes]] array
     * @return string the rendering result
     */
    protected function renderAttribute($attribute, $index)
    {
        $odd = ($index % 2) > 0;
        $odd = $odd ? 'odd': 'even';
        if (is_string($this->template)) {

            $options =  $this->labelOptions;//ArrayHelper::getValue($attribute, 'captionOptions', []);
            $tag = ArrayHelper::remove($options, 'tag', 'div');
            $options['class']= (isset($options['class'])) ? $options['class'].' '.$odd : $odd;
            $label = Html::tag($tag,$attribute['label'], $options);

            $options =  $this->valueOptions;//ArrayHelper::getValue($attribute, 'captionOptions', []);
            $tag = ArrayHelper::remove($options, 'tag', 'div');
            $options['class']= (isset($options['class'])) ? $options['class'].' '.$odd : $odd;
            $value = Html::tag($tag,$this->formatter->format($attribute['value'], $attribute['format']), $options);


            return strtr($this->template, [
                '{label}' => $label,
                '{value}' => $value,
            ]);
        } else {
            return call_user_func($this->template, $attribute, $index, $this);
        }
    }

}
