<?php
/**
 * Created by PhpStorm.
 * User: rw
 * Date: 9/11/2017
 * Time: 6:46 PM
 */

namespace app\components\models;


use app\components\MyHelper;
use MongoDB\BSON\UTCDateTime;
use yii\base\Model;
use Yii;
use Yii\helpers\ArrayHelper;

class delete2DynModel extends BaseActiveRecord
{
    Const UNDEFINED_VALUE = '';
    protected $dynamicFields = [];
    protected $dynamicRules = [];

    public function __construct(array $config = [])
    {
        parent::__construct($config);
        //$this->validate();
        //$this->clearErrors();
    }

   public function fields(){
        return array_keys($this->getDefaultValues());
    }

    public function getDefaultValues() {
        return [];
    }

    public function rules()
    {
        $defValues = $this->getDefaultValues();
        $fields = array_keys($defValues);
        $defRules = [];
        if(count($fields)>0) {
            $defRules[] = [
                $fields, 'default', 'value' => function ($model, $attribute) {
                    $defValues = $model->getDefaultValues();
                    return $defValues[$attribute];
                }

            ];
            $parentRules= parent::rules();
            return (count($parentRules)>0) ? ArrayHelper::merge($defRules,$parentRules) : $defRules;
        }else{
            return parent::rules();
        }

    }

    public function setDynRules($rule) {
        $this->dynamicRules = $rule;
    }

    private $phpSufix='2';
    public function __get($name)
    {
        $getter = 'get' . $name;
        $phpDate=$name.$this->phpSufix;

        if (method_exists($this, $getter)) {
            // read property, e.g. getName()
            $ret = $this->$getter();
        } elseif (isset($this->dynamicFields[$name])) {
            $ret = $this->dynamicFields[$name];
        } elseif (! isset($this->$name) &&
                    isset($this->$phpDate) &&
                    is_numeric($this->$phpDate) ) {
            return MyHelper::phpDate2MongoDate($this->$phpDate);
        } elseif (($values = method_exists($this,"getDefaultValues") ? $this->getDefaultValues() : [])
                && !isset($this->$name) && in_array($name,array_keys($values))){
                $this->dynamicFields[$name] = $values[$name];
                $ret= $values[$name];
        }else try{
            $ret = parent::__get($name);
        }catch(\Exception $e){
            $ret=self::UNDEFINED_VALUE;
        }

        if($ret instanceof UTCDateTime) {
            $this->$phpDate=MyHelper::mongoDate2PhpDate($ret);
        }

        return $ret;
    }

    public function __set($name, $value)
    {
        $setter = 'set' . $name;
        $phpDate=$name.$this->phpSufix;

        if (method_exists($this, $setter)) {
            // set property
            $this->$setter($value);
            $ret = $this->{$name};
        } elseif (!isset($this->{$name})) {
            $this->dynamicFields[$name] = $value;
            $ret = $value;
        } elseif (isset($this->dynamicFields[$name])) {
            $this->dynamicFields[$name] = $value;
            $ret = $value;
            //check if is date
            if ($value instanceof UTCDateTime) {
                $this->$phpDate = MyHelper::mongoDate2PhpDate($value);
            }
        } else {
            $ret = parent::__set($name, $value);
        }
        if($value instanceof UTCDateTime) {
            $name.=$this->phpSufix;
            $this->$name = MyHelper::mongoDate2PhpDate( $value );
        }
        return $ret;
    }

}