<?php
/**
 * Created by PhpStorm.
 * User: vitorcool
 * Date: 10/4/2017
 * Time: 9:50 PM
 */

namespace app\components\models;


interface DynModelInterface
{
    /**
     * @return array
     */
    function getDefaultValues();

    /**
     * @param string $name
     * @return mixed
     */
    function getDefaultValue($name);

    /**
     * @param array $value
     */
    function setDefaultValues( $value );

    /**
     * @param string $name
     * @param mixed $value
     */
    function setDefaultValue($name,$value);


}