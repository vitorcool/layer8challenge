<?php
/**
 * Created by PhpStorm.
 * User: vitor oliveira
 * Date: 9/11/2017
 * Time: 11:29 AM
 */

namespace app\components\models;

use yii\db\Exception;
use yii\helpers\ArrayHelper;

class ActiveRecord extends ActiveRecordEmbed {
//}class _xx extends ActiveRecordEmbed{
    protected $dynamicFields = [];
    Const UNDEFINED_VALUE = '';

    public function __construct(array $config = [])
    {
        parent::__construct($config);
    }

    public function fields(){
        return array_keys($this->getDefaultValues());
    }

    public function getDefaultValues() {
        return [];
    }

    public function rules()
    {
        $defValues = $this->getDefaultValues();
        $fields = array_keys($defValues);
        $defRules = [];
        if(count($fields)>0) {
            $defRules[] = [
                $fields, 'default', 'value' => function ($model, $attribute) {
                    $defValues = $this->getDefaultValues();
                    return $defValues[$attribute];
                }
            ];
            $parentRules= parent::rules();
            return (count($parentRules)>0) ? ArrayHelper::merge($defRules,$parentRules ) : $defRules;
        }else{
            return parent::rules();
        }

    }


    public function __get($name)
    {
        try {
            $ret = parent::__get($name);
        }catch(\Exception $e){
            if (isset($this->dynamicFields[$name])) {
                $ret = $this->dynamicFields[$name];
                /* get default values if there is any */
            } elseif (($values = method_exists($this,"getDefaultValues") ? $this->getDefaultValues() : [])
                && !isset($this->$name) && in_array($name,array_keys($values))){
                $this->dynamicFields[$name] = $values[$name];
                $ret= $values[$name];
            } else
                $ret = self::UNDEFINED_VALUE;
        }


       // $ret=$this->getAttribute($name);
       // if(!isset($ret))


        return $ret;
    }

    /**
     * Use throwed exception to create dynamicaly new properties
     * @param string $name
     * @param mixed $value
     * @return mixed|void
     */
    public function __set($name, $value)
    {
        try{
            $ret = parent::__set($name, $value);
        }catch(Exception $e){
            // erro trying to set unexisting property
            // lets add it dynamicaly - How?
            $this->dynamicFields[$name] = $value;
            $ret=$value;
        }
        return $ret;

    }
}