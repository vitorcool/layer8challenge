<?php

namespace app\components;

use Yii;

class ActionColumn extends \yii\grid\ActionColumn
{
    public $buttonOptions = ['class'=>'btn btn-primary'];
}
