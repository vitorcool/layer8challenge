<?php

namespace app\components;

use yii\i18n\MissingTranslationEvent;
use \Statickidz\GoogleTranslate;

class AutoTranslationEventHandler
{
    public static function handleMissingTranslation(MissingTranslationEvent $event)
    {
        $trans = new GoogleTranslate();
        $result = $trans->translate(\Yii::$app->language, $event->language, $event->message);
        $event->translatedMessage = !empty($result) ? $result : "($event->language){$event->message}";
    }


}