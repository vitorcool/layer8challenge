<?php
/**
 * Created by PhpStorm.
 * User: rw
 * Date: 9/7/2017
 * Time: 5:12 PM
 */

namespace app\components;


use MongoDB\BSON\UTCDateTime;
use Yii;

class MyHelper
{
    static function initials( $phrase){
        $words= explode(' ',$phrase);
        $ret = '';
        foreach($words as $word)
            $ret .= strtoupper( substr($word,0,1));
        return $ret;
    }

    /**
     * @param \MongoDB\BSON\ObjectID|string $id
     * @return string
     */
    static function fix_id( $id ){
        if( is_array($id)){
            $arrId=$id;
            $id='';
            foreach($arrId as $name => $key)
                $id.=$key;
        }
        return (string) $id;
    }

    /**
     * @param UTCDateTime $date
     * @return string
     */
    static function mongoDate2PhpDate( $date ){
        if( $date instanceof UTCDateTime ){
            return $date->toDateTime()->getTimestamp();
        }
        return $date;
    }

    /**
     * @param integer $date //timeStamp
     */
    static function phpDate2MongoDate( $date ){
        if(is_numeric($date) || ($date % 2)==0 ) {
            return new UTCDateTime($date*1000);
        }
        return $date;
    }

    /**
     * @param string    $str           Original string
     * @param string    $needle        String to trim from the end of $str
     * @param bool|true $caseSensitive Perform case sensitive matching, defaults to true
     * @return string Trimmed string
     */
    static function rightTrim($str, $needle, $caseSensitive = true)
    {
        $strPosFunction = $caseSensitive ? "strpos" : "stripos";
        if ($strPosFunction($str, $needle, strlen($str) - strlen($needle)) !== false) {
            $str = substr($str, 0, -strlen($needle));
        }
        return $str;
    }

    /**
     * @param string    $str           Original string
     * @param string    $needle        String to trim from the beginning of $str
     * @param bool|true $caseSensitive Perform case sensitive matching, defaults to true
     * @return string Trimmed string
     */
    static function leftTrim($str, $needle, $caseSensitive = true)
    {
        $strPosFunction = $caseSensitive ? "strpos" : "stripos";
        if ($strPosFunction($str, $needle) === 0) {
            $str = substr($str, strlen($needle));
        }
        return $str;
    }

    static function isAjax(){
        $ajax = Yii::$app->request->get('ajax');
        return $ajax==true;
    }

    static function strZero( $num , $len ){
        $num = $num.'';
        while (strlen($num)< $len)
            $num= "0".$num;
        return $num;
    }

    static function startsWith($haystack, $needle)
    {
        $length = strlen($needle);
        return (substr($haystack, 0, $length) === $needle);
    }

    static function endsWith($haystack, $needle)
    {
        $length = strlen($needle);

        return $length === 0 ||
            (substr($haystack, -$length) === $needle);
    }
}