<?php

namespace app\models;

use app\components\models\ActiveRecord;
use app\components\MyHelper;
use MongoDB\BSON\UTCDateTime;
use Yii;

/**
 * This is the model class for collection "restaurants".
 *
 * @property \MongoDB\BSON\ObjectID|string $_id
 * @property mixed $restaurant_id
 * @property mixed $name
 * @property mixed $cuisine
 * @property mixed $borough
 * @property mixed $address
 * @property mixed $comments

 */
class Restaurant extends ActiveRecord
{
    public $addressData=[];
    public $gradesData=[];
    public $commentsData=[];

    /**
     * @return \MongoDB\BSON\ObjectID|string
     */
    public function getId()
    {
        return MyHelper::fix_id($this->_id);
    }

    /**
     * @inheritdoc
     */
    public static function collectionName()
    {
        return ['challenge8', 'restaurants'];
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            '_id',
            'restaurant_id',
            'name',
            'cuisine',
            'borough',
            'address',
            'grades',
            'comments',
        ];
    }

    /**
     * @return \yii2tech\embedded\Mapping
     */
    public function embedAddress()
    {
        return $this->mapEmbedded('addressData', RestaurantAddress::className());
    }

    /**
     * @return \yii2tech\embedded\Mapping
     */
    public function embedGrades()
    {
        return $this->mapEmbeddedList('gradesData', RestaurantGrades::className());
    }

    /**
     * @return \yii2tech\embedded\Mapping
     */
    public function embedComments()
    {
        return $this->mapEmbeddedList('commentsData', RestaurantComments::className());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['restaurant_id', 'name', 'cuisine', 'borough'], 'safe'],
            [['restaurant_id', 'name', 'cuisine', 'borough','address'], 'required'],
            [['restaurant_id'],'validateUnchangeable', 'on'=>'update'],
            [['restaurant_id', 'name', 'cuisine', 'borough'], 'string', 'max'=>100 ],
            [['restaurant_id'], 'unique', 'on'=>'create'],
            //[['address','grades'], 'unsafe'],
            [['address'], 'yii2tech\embedded\Validator'],
            [['grades','comments'], 'yii2tech\embedded\Validator'],
        ];
    }

    public function validateUnchangeable($attribute, $params)//, $validator)
    {
        if ($this->$attribute!== $this->oldAttributes[$attribute] ) {
            $errMess =  Yii::t('restaurant','{attLabel} can not be changed.',
                                ['attLabel'=>$this->getAttributeLabel($attribute)]);
            $this->addError($attribute, $errMess);
            return false;
        }
        return true;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            '_id' => Yii::t('restaurant', 'ID'),
            'restaurant_id' => Yii::t('restaurant', 'Restaurant ID'),
            'name' => Yii::t('restaurant', 'Name'),
            'cuisine' => Yii::t('restaurant', 'Cuisine'),
            'borough' => Yii::t('restaurant', 'Borough'),
            'address', Yii::t('restaurant', 'Address'),
            'grades', Yii::t('restaurant', 'Grades'),
            'comments', Yii::t('restaurant', 'Comments'),
        ];
    }

    static public function countGradesPerMonth(){
        $pastYears = 4;
        $dat=strtotime('-4 year');

        $conn=Yii::$app->mongodb;
        $pipe=[

            ['$unwind' => '$grades'],
            ['$match' => [
                "grades.date" => [
                    '$gte'=> new UTCDateTime($dat*1000)
                ]
            ]],
            ['$project' => [
                "year"  => ['$year'  => '$grades.date'],
                "month" => ['$month' => '$grades.date'],
                "score" => '$grades.score'

            ]],
            ['$group' => [
                "_id" => ["year" => '$year',"month" => '$month' ],
                "count" => ['$sum'=>1],
                "score" => ['$sum'=>'$score']
            ]],
            ['$sort' => [ "_id" => 1 ]],
        ];

        $ret=$conn->createCommand()->aggregate('restaurants',$pipe);

        // change result format
        // add missing months do result


        $x = array_map( function( $id ){
                return $id['year'].'-'.$id['month'];
            }, array_column($ret,'_id'));


        $f = reset($ret);
        $l = end($ret);

        $year_month =[];
        $count=[];
        $sum=[];
        for($y=$f['_id']['year']; $y<=$l['_id']['year'] ; $y++) {
            for ($m = (($y == $f['_id']['year']) ? $l['_id']['month'] : 1);
                 $m <= (($y == $l['_id']['year']) ? $l['_id']['month'] : 12);
                 $m++) {
                $year_month[] = $y . '-' . MyHelper::strZero($m,2);
                $pos = array_search($y . '-' . $m, $x);
                if (gettype($pos)=="integer") {
                    $count[] = $ret[$pos]['count'];
                    $sum[]   = $ret[$pos]['score'];
                }else{
                    $count[] = 0;
                    $sum[]   =0;
                }
            }
        }


        return [
            'year_month'=> $year_month,
            'count'     => $count,
            'sum'       => $sum];

    }

    static public function countCommentsPerDay($pastDays = 7)
    {
        $dat=strtotime("-$pastDays days");
        $conn = Yii::$app->mongodb;
        $pipe = [
            ['$match' =>
                [
                    "comments.date" => ['$exists' => true ],
                    "comments.date" => [
                        '$gte' => new UTCDateTime($dat * 1000)
                    ]
                ],

            ],
            ['$unwind' => '$comments'],
            ['$project' => [
                "year" => ['$year' => '$comments.date'],
                "month" => ['$month' => '$comments.date'],
                "day"   => ['$dayOfMonth' => '$comments.date'],
            ]],
            ['$group' => [
                "_id" => ["year" => '$year', "month" => '$month', "day" => '$day'],
                "count" => ['$sum' => 1],
            ]],
            ['$sort' => ["_id" => -1]],
        ];

        $ret = $conn->createCommand()->aggregate('restaurants', $pipe);

        $x = array_map(function ($id) {
            return $id['year'] . '-' . $id['month'] . '-' . $id['day'];
        }, array_column($ret, '_id'));

        $f = end($ret);
        $l = reset($ret);

        $year_month = [];
        $count = [];


        for ($y = $f['_id']['year']; $y <= $l['_id']['year']; $y++) {
            for ($m = (($y == $f['_id']['year']) ? $l['_id']['month'] : 1);
                 $m <= (($y == $l['_id']['year']) ? $l['_id']['month'] : 12);
                 $m++) {
                for($d = ($y==$f['_id']['year'] && $m==$f['_id']['month']) ? $f['_id']['day'] : 1;
                    $d <= (($y==$l['_id']['year'] && $m==$l['_id']['month']) ? $l['_id']['day'] : cal_days_in_month(CAL_GREGORIAN, $m, $y));
                    $d++) {
                    $year_month[] = $y . '-' . $m . '-' . $d;
                    $pos = array_search($y . '-' . $m . '-' . $d, $x);
                    if (gettype($pos) == "integer") {
                        $count[] = $ret[$pos]['count'];
                    } else {
                        $count[] = 0;
                    }
                }
            }
        }

        return [
            'year_month' => $year_month,
            'count' => $count,
        ];
    }

    static public function timeBetweenComments($pastDays=1) {
        $dat=strtotime("-$pastDays day");

        $conn=Yii::$app->mongodb;
        $pipe=[
                [ '$unwind' => '$comments' ],
                [ '$match' => [
                    "comments.date" => [
                        '$exists' => true
                    ],
                    "comments.date" => [
                        '$gte' =>  new UTCDateTime($dat*1000)
                    ]
                ]],
                ['$project' => [
                    'date' => '$comments.date'
                ]],
                ['$sort' => [ "date" => -1 ]],
           //     ['$limit' => 200]
            ];
        $ret=$conn->createCommand()->aggregate('restaurants',$pipe);

        $difs =[];
        $last=null;
        foreach($ret as $item){
            $date=$item['date'];
            if(!$date instanceof UTCDateTime)
                continue;
            $date=$date->toDateTime()->getTimestamp();
            if(!is_null($last))
                $difs[]=$last-$date ;
            $last=$date;
        }
        $max=0;
        $min=99999999999999999999999;
        $sum=0;
        foreach($difs as $dif){
            $max=max($max,$dif);
            $min=min($min,$dif);
            $sum+=$dif;
        }
        $avg = $sum/count($difs);


        return ['max'=>$max, 'avg'=>$avg, 'min'=> $min];
    }
}
