<?php
/**
 * Created by PhpStorm.
 * User: rw
 * Date: 9/11/2017
 * Time: 11:37 AM
 */

namespace app\models\ES;


/**
 * This is the model class for collection "restaurantGrades".
 *
 * @property \MongoDB\BSON\UTCDateTime|string $date
 * @property string $grade
 * @property integer $score
 */

use app\components\models\DynModel;
use app\components\MyHelper;
use MongoDB\BSON\UTCDateTime;
use mongosoft\mongodb\MongoDateBehavior;
use yii\base\Model;
use Yii;
use Yii\helpers\ArrayHelper;
use yii\db\BaseActiveRecord;

class RestaurantGrades extends DynModel
{
    const SCORES = ["A"=>"*","B"=>"**","C"=>"***","P"=>"****","Z"=>"*****"];



//    public $date;
    public $date2;
//    public $grade;
//    public $score;

    public function __construct(array $config = [])
    {
        $this->setDefaultValues([
            'date'=> new UTCDateTime(time()*1000),
            'grade'=> '',
            'score'=>0,
        ]);
        parent::__construct($config);
    }

    public function dynRules()
    {
        $rules=[
                [['date2','grade','score'], 'safe'],
                [['date2','grade','score'], 'required'],
                [['date2'], 'date','format'=>'php:U'],
                [['grade'], 'in', 'range' => array_keys(self::SCORES)],
                [['score'], 'integer', 'integerOnly' => true, 'max'=>200],
            ];
        return $rules;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'date'  => Yii::t('restaurant', 'Date'),
            'date2'  => Yii::t('restaurant', 'Date'),
            'grade' => Yii::t('restaurant', 'Grade'),
            'score' => Yii::t('restaurant', 'Score'),

        ];
    }
}