<?php

namespace app\models;

use app\components\MyHelper;
use MongoDB\BSON\UTCDateTime;
use Yii;
use app\components\models\ActiveRecord;

/**
 * This is the model class for collection "site_contact".
 *
 * @property \MongoDB\BSON\ObjectID|string $_id
 * @property mixed $name
 * @property mixed $email
 * @property mixed $subject
 * @property mixed $body
 * @property UTCDateTime $date
 */
class SiteContact extends ActiveRecordES
{
    const SCENARIO_USER_IS_GUEST    = 'user_is_guest';

    public $date2;
    public $verify_code="";

    /**
     * @return \MongoDB\BSON\ObjectID|string
     */
    public function getId()
    {
        return MyHelper::fix_id($this->_id);
    }

    public static function index()
    {
        return 'challenge8';//Inflector::pluralize(Inflector::camel2id(StringHelper::basename(get_called_class()), '-'));
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            '_id',
            'name',
            'email',
            'subject',
            'body',
            'date',
        ];
    }

    public function getDefaultValues()
    {
        return [
            'name'=>'',
            'email'=>'',
            'subject'=>'',
            'body'=>'',
            'date'=>new UTCDateTime(time()*1000),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            '_id' => Yii::t('site', 'ID'),
            'name' => Yii::t('site', 'Name'),
            'email' => Yii::t('site', 'Email'),
            'subject' => Yii::t('site', 'Subject'),
            'body' => Yii::t('site', 'Body'),
            'verify_code' => Yii::t('site', 'Verification Code'),
            'date' => Yii::t('site', 'Date'),
        ];
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        $ret= array_merge(parent::rules(),[
            [['name', 'email', 'subject', 'body', 'date','verify_code'], 'safe'],
            // name, email, subject and body are required
            [['name', 'email', 'subject', 'body'], 'required'],
            [['date2'], 'date','format'=>'php:U'],
            // email has to be a valid email address
            ['email', 'email'],
            // verify_code needs to be entered correctly
            [['verify_code'], 'captcha', 'on'=> self::SCENARIO_USER_IS_GUEST],
        ]);
        return $ret;
    }

    public function scenarios()
    {
        return [
            "default" =>                        ['name','email','subject','body','date'],
            self::SCENARIO_USER_IS_GUEST    => ['name','email','subject','body','date','verify_code'],
            ];
    }


    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @param string $email the target email address
     * @return bool whether the model passes validation
     */
    public function contact($email)
    {

        if (true) {
            Yii::$app->mailer->compose()
                ->setTo($email)
                ->setFrom([$this->email => $this->name])
                ->setSubject($this->subject)
                ->setTextBody($this->body)
                ->send();

            return true;
        }
        return false;
    }
}
