<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * RestaurantSearch represents the model behind the search form about `app\models\Restaurant`.
 */
class RestaurantSearch2 extends Restaurant
{

    public $restaurant;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['restaurant'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Restaurant::find()->orderBy(['name'=>SORT_ASC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
     /*       'sort' => [
                'defaultOrder' => [
                    'name' => SORT_DESC,
                ]
            ],*/
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $a=$this->restaurant;
        $query
            ->orFilterWhere(['_id' => $this->restaurant])
            ->orFilterWhere(['like', 'restaurant_id', $this->restaurant])
            ->orFilterWhere(['like', 'name', $this->restaurant])
            ->orFilterWhere(['like', 'cuisine', $this->restaurant])
            ->orFilterWhere(['like', 'borough', $this->restaurant])
            ->orFilterWhere(['like', 'address.building', $this->restaurant])
            ->orFilterWhere(['like', 'address.street', $this->restaurant])
            ->orFilterWhere(['like', 'address.zipcode', $this->restaurant]);

        $dataProvider->setSort([
            'attributes'=>[
                'restaurant'=>
                    [   'asc'  => ["address.name" => SORT_ASC ],
                        'desc' => ["address.name" => SORT_DESC ],
                    ],
                'address'=>
                    [   'asc'  => ["address.street" => SORT_ASC ],
                        'desc' => ["address.street" => SORT_DESC ],
                    ],
            ]
        ]);


        return $dataProvider;
    }
}
