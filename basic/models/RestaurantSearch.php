<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * RestaurantSearch represents the model behind the search form about `app\models\Restaurant`.
 */
class RestaurantSearch extends Restaurant
{

    public $street;
    public $zipcode;
    public $coord;
    public $building;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['_id', 'restaurant_id', 'name', 'cuisine', 'borough','street','building','zipcode'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Restaurant::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->orFilterWhere(['_id'=> $this->_id])
            ->andFilterWhere(['like', 'restaurant_id', $this->restaurant_id])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'cuisine', $this->cuisine])
            ->andFilterWhere(['like', 'borough', $this->borough])
            ->andFilterWhere(['like', 'address.building', $this->building])
            ->andFilterWhere(['like', 'address.street', $this->street])
            ->andFilterWhere(['like', 'address.zipcode', $this->zipcode]);

        $dataProvider->setSort([
            'attributes'=>[
                'address.street'=>
                    [   'asc'  => ["address.street" => SORT_ASC ],
                        'desc' => ["address.street" => SORT_DESC ],
                    ],
                'address.zipcode'=>
                    [   'asc'  => ["address.zipcode" => SORT_ASC ],
                        'desc' => ["address.zipcode" => SORT_DESC ],
                    ],
                'address.building'=>
                    [   'asc'  => ["address.building" => SORT_ASC ],
                        'desc' => ["address.building" => SORT_DESC ],
                    ],
            ]
        ]);


        return $dataProvider;
    }
}
