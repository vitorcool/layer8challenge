<?php
/**
 * Message translations.
 *
 * This file is automatically generated by 'yii message/extract' command.
 * It contains the localizable messages extracted from source code.
 * You may modify this file by translating the extracted messages.
 *
 * Each array element represents the translation (value) of a message (key).
 * If the value is empty, the message is considered as not translated.
 * Messages that no longer need translation will have their translations
 * enclosed between a pair of '@@' marks.
 *
 * Message string can be used with plural forms format. Check i18n section
 * of the guide for details.
 *
 * NOTE: this file must be saved in UTF-8 encoding.
 */
return [
    'Access Token' => 'Token de acceso',
    'Are you sure you want to delete this item?' => '¿Estás seguro de que quieres eliminar este elemento?',
    'Auth Key' => 'Clave de autenticación',
    'Create' => 'Crear',
    'Create User' => 'Crear usuario',
    'Deactivated' => 'Desactivado',
    'Delete' => 'Borrar',
    'Email' => 'Email',
    'ID' => 'CARNÉ DE IDENTIDAD',
    'Is Admin' => 'Es admin',
    'Login' => 'Iniciar sesión',
    'Name' => 'Nombre',
    'Password' => 'Contraseña',
    'Pending email confirmation' => 'Confirmación de correo electrónico pendiente',
    'Please fill out the following fields to login:' => 'Por favor, rellene los siguientes campos para ingresar:',
    'Register' => 'Registro',
    'Register User' => 'Registrar usuario',
    'Registered' => 'Registrado',
    'Remenber Me' => 'Recuérdame',
    'Reset' => 'Reiniciar',
    'Search' => 'Buscar',
    'State' => 'Estado',
    'Update' => 'Actualizar',
    'Update {modelClass}' => 'Actualizar {modelClass}',
    'User' => 'Usuario',
    'Users' => 'Usuarios',
];
