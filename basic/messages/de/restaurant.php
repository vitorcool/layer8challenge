<?php
/**
 * Message translations.
 *
 * This file is automatically generated by 'yii message/extract' command.
 * It contains the localizable messages extracted from source code.
 * You may modify this file by translating the extracted messages.
 *
 * Each array element represents the translation (value) of a message (key).
 * If the value is empty, the message is considered as not translated.
 * Messages that no longer need translation will have their translations
 * enclosed between a pair of '@@' marks.
 *
 * Message string can be used with plural forms format. Check i18n section
 * of the guide for details.
 *
 * NOTE: this file must be saved in UTF-8 encoding.
 */
return [
    'User' => 'Benutzer',
    '' => '(de)',
    'Address' => 'Adresse',
    'Are you sure you want to delete this item?' => 'Sind Sie sicher, dass Sie diesen Artikel löschen möchten?',
    'Avg' => 'Durchschn',
    'Borough' => 'Bezirk',
    'Building' => 'Gebäude',
    'Comment' => 'Kommentar',
    'Comments' => 'Bemerkungen',
    'Comments Analises' => 'Kommentare Analysen',
    'Comments stats' => 'Kommentare stats',
    'Coordinates' => 'Koordinaten',
    'Create' => 'Erstellen',
    'Create Comment about restaurant {name}' => 'Kommentar über restaurant {name} erstellen',
    'Create Comments' => 'Kommentare erstellen',
    'Create Grade to restaurante {name}' => 'Erstellung Grade to restaurante {name}',
    'Create Restaurant' => 'Restaurant erstellen',
    'Cuisine' => 'Küche',
    'Date' => 'Datum',
    'Delete' => 'Löschen',
    'Email' => 'Email',
    'Grade' => 'Klasse',
    'Grades' => 'Noten',
    'Grades Analises' => 'Notenanalysen',
    'ID' => 'ICH WÜRDE',
    'Intervalo entre mensagens' => 'Intervalo entre mensagens',
    'Location Map of restaurant "{name}"' => 'Lageplan des Restaurants "{name}"',
    'Max' => 'Max',
    'Min' => 'Min',
    'Monthly Grades Analises' => 'Monatliche Notenanalysen',
    'More Information' => 'Mehr Informationen',
    'Name' => 'Name',
    'Reset' => 'Zurücksetzen',
    'Restaurant ID' => 'Restaurant-ID',
    'Restaurant location Map' => 'Restaurant Lage Karte',
    'Restaurants' => 'Restaurants',
    'Restaurants Search' => 'Restaurants suchen',
    'Score' => 'Ergebnis',
    'Search' => 'Suche',
    'Show comments' => 'Kommentare anzeigen',
    'Showing <b>{begin}-{end}</b> of <b>{totalCount}</b> items.' => 'Zeigen <b> {begin} - {end} </ b> von <b> {totalCount} </ b> Items.',
    'Statistic time between comments' => 'Statistikzeit zwischen Kommentaren',
    'Street' => 'Straße',
    'Time (seconds)' => 'Zeit (Sekunden)',
    'Total counted grades' => 'Gesamtzahl der Noten',
    'Total summed score' => 'Gesamtsumme',
    'Update' => 'Aktualisieren',
    'Update Comment ({idx}) of {name}' => 'Kommentar aktualisieren ({idx}) von {name}',
    'Update Grade ({idx}) of {name}' => 'Update Grade ({idx}) von {name}',
    'Update {modelClass}: ' => 'Update {modelClass}:',
    'User ID' => 'Benutzeridentifikation',
    'You do not have permition to update this comment.' => 'Sie haben keine Berechtigung, diesen Kommentar zu aktualisieren.',
    'Zipcode' => 'Postleitzahl',
    'comments counted' => 'Kommentare gezählt',
    'day' => 'Tag',
    'grades counted' => 'Noten gezählt',
    'month' => 'Monat',
    'quantity' => 'Anzahl',
    'score sumed' => 'Ergebnis gesammelt',
    'time (seconds)' => 'Zeit (Sekunden)',
    '{attLabel} can not be changed.' => '{attLabel} kann nicht geändert werden.',
];
