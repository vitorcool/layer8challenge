<?php
/**
 * Message translations.
 *
 * This file is automatically generated by 'yii message/extract' command.
 * It contains the localizable messages extracted from source code.
 * You may modify this file by translating the extracted messages.
 *
 * Each array element represents the translation (value) of a message (key).
 * If the value is empty, the message is considered as not translated.
 * Messages that no longer need translation will have their translations
 * enclosed between a pair of '@@' marks.
 *
 * Message string can be used with plural forms format. Check i18n section
 * of the guide for details.
 *
 * NOTE: this file must be saved in UTF-8 encoding.
 */
return [
    'About' => 'Über',
    'Body' => 'Körper',
    'Contact' => 'Kontakt',
    'Date' => 'Datum',
    'Email' => 'Email',
    'ID' => 'ICH WÜRDE',
    'If you have business inquiries or other questions, please fill out the following form to contact us. <br> Thank you.' => 'Wenn Sie geschäftliche Anfragen oder andere Fragen haben, füllen Sie bitte das folgende Formular aus, um uns zu kontaktieren. <br> Vielen Dank',
    'Name' => 'Name',
    'Subject' => 'Fach',
    'Submit' => 'einreichen',
    'Thank you for contacting us. We will respond to you as soon as possible.' => 'Danke, dass Sie uns kontaktiert haben. Wir werden Ihnen so schnell wie möglich antworten.',
    'This is the About page.' => 'Dies ist die Info-Seite.',
    'Verification Code' => 'Bestätigungscode',
];
