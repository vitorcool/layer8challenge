<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use \app\components\MyHelper;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <link rel='stylesheet' type='text/css' href='<?php echo $this->theme->baseUrl; ?>/css/site.css'  />
    <link rel='stylesheet' type='text/css' href='<?php echo $this->theme->baseUrl; ?>/css/<?= Yii::$app->params['bootstrap_theme'] ?>.css'  />

</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::t('layout', 'My Company'),
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar navbar-fixed-top navbar-default',        ],
    ]);

    $label_contact = ['label' => Yii::t('layout', 'Contact'), 'url' => '#','options'=>[
        'class'=>'load-modal',
        'data-url'  => \yii\helpers\Url::toRoute(['site/contact'], true),
        "data-title"=> Yii::t('site','Contact'),
    ]];
    $label_about = ['label' => Yii::t('layout', 'About'), 'url' => ['/site/about']];
    $label_CRUD_restaurants= ['label' => Yii::t('layout', 'Users'), 'url' => ['/user'],'visible'=>\app\models\User::isAdmin()];
    $label_CRUD_users = ['label' => Yii::t('layout', 'Restaurant'), 'url' => ['/restaurant'],'visible'=>\app\models\User::isAdmin()];

    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-left'],
        'items' => [
            //['label' => Yii::t('layout', 'Home'), 'url' => ['/site/index']],
            ['label' => Html::encode(Yii::t('layout','Options')),'url' => '#',
                "linkOptions" => ["class"=>"dropdown-toggle","data-toggle"=>"dropdown"],
                "options" => [ "class" => "dropdown visible-sm"],
                "items" => [
                    $label_CRUD_users,
                    $label_CRUD_restaurants,
                    array_merge($label_contact,['visible'=>Yii::$app->user->isGuest]),
                    array_merge($label_about,['visible'=>Yii::$app->user->isGuest]),
                ],
            ],
            array_merge($label_CRUD_users,['options'=>['class'=>'visible-xs visible-md visible-lg']]),
            array_merge($label_CRUD_restaurants,['options'=>['class'=>'visible-xs visible-md visible-lg']]),
            array_merge($label_contact,['visible'=>Yii::$app->user->isGuest]),
            array_merge($label_about,['visible'=>Yii::$app->user->isGuest]),
        ]
    ]);?>
    <form class="navbar-form navbar-right">
        <input type="text" class="form-control col-lg-8" placeholder="<?php echo Html::encode(Yii::t('layout','Search food and restaurants'))?>">
    </form>
    <?php
    /* begin menu lang*/
    $langs = ['label' => Html::encode(Yii::$app->language),
                "linkOptions" => ["class"=>"dropdown-toggle","data-toggle"=>"dropdown"], //'url' => '#',
                "items" => [
                ],
            ];
    foreach( Yii::$app->params['languages'] as $name){
        if($name!==Yii::$app->language)
            $langs['items'][] = ['label' => $name, 'url' => \yii\helpers\Url::current(['lang'=>urlencode($name)])];
    }
    /* end menu lang */
    /* begin menu theme*/
    $tCssName = Html::encode(Yii::$app->params['bootstrap_theme']);
    $tName    = array_search ($tCssName, Yii::$app->params['bootstrap_themes']);
    $themes = ['label' => $tName,
                "linkOptions" => ["class"=>"dropdown-toggle","data-toggle"=>"dropdown"],//'url' => '#',
                "items" => [],
            ];
    foreach( Yii::$app->params['bootstrap_themes'] as $name=>$cssFilename){
        if($name!==Yii::$app->params['bootstrap_theme'])
            $themes['items'][] = ['label' => $name, 'url' => \yii\helpers\Url::current(['theme'=>urlencode($cssFilename)])];
    }

    if(Yii::$app->user->isGuest)
        $items = [
            $themes,
            $langs,
            ['label' => Yii::t('layout', 'Login'), 'url' => ['/site/login']]
        ];
    else {
        $items = [
            [
                'label' => MyHelper::initials(Yii::$app->user->identity->username), //'url' => ['#'],
                "linkOptions" => ["class"=>"dropdown-toggle","data-toggle"=>"dropdown"],
                "options" => [ "class" => "dropdown"],
                'dropDownOptions' => [ "class" => "dropdown dropdown-menu"],
                "items" => [
                    $themes,
                    $langs,
                    '<li class="divider"></li>',
                    $label_contact,
                    $label_about,
                    '<li class="divider"></li>',
                    ['label' => Yii::t('layout', 'Logout'), 'url' => ['/site/logout']]
                ],
            ],

        ];
    }
    /* end menu theme */
    echo Nav::widget([
        'options' => [ "class" => "nav navbar-nav navbar-right",  ],
        'items' => $items,

    ]);
    ?>


    <?php NavBar::end(); ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>

        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; <?= Yii::t('layout', 'My Company').' '. date('Y') ?></p>

        <p class="pull-right" style="display: none"><?= Yii::powered() ?></p>
    </div>
</footer>
<?php
yii\bootstrap\Modal::begin([
    'headerOptions' => ['id' => 'modalHeader'],
    'id' => 'appModal',
    'size' => 'modal-lg',
    //keeps from closing modal with esc key or by clicking out of the modal.
    // user must click cancel or X to close
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
]);?>
<div id="modalContent" class="container-fluid">
    <div style="text-align:center;padding-top: 100px;padding-bottom: 100px"><img src="/img/loader.svg"></div>
</div>
<?php
yii\bootstrap\Modal::end();
?>
<?php
$urlGradeUpdate=\yii\helpers\Url::toRoute(['restaurant/grade-details'],true);
$script = <<< JS
    $(function(){
        $('.modal-dialog').draggable({
            cancel: ".modal-body"
        });
        
        $('#restModal').on('show.bs.modal', function () {
            $(this).find('.modal-body').css({
                'max-height':'100%'
            });
        });

        $(document).on('click', '.load-modal', function(e){
            var targetModal = $("#appModal");
            var loadingUrl  = $(this).data("url");
            var title       = $(this).data("title");        
            if(loadingUrl.length==0) return;
            
            if (!$(targetModal).data('bs.modal').isShown) {
                $(targetModal).find("#modalContent").html('<div style="text-align:center;padding-top: 100px;padding-bottom: 100px"><img src="/img/loader.svg"></div>');
                $(targetModal).modal('show');
            }      
                                               
            $(targetModal).find('#modalContent')
            .load(loadingUrl,null,function( responseText, textStatus, xhr ){
                if (textStatus !== 'success') {
                    var errormsg;
                    if (xhr && xhr.statusText)
                        errormsg = xhr.statusText;
                    if (errormsg === '' || errormsg === null)
                        errormsg = $(responseText).text();
            
                    errormsg += ' at ' + (new Date()).toLocaleTimeString();
            
                    $('#messageregion').html(errormsg);
                    $(targetModal).modal('hide');
                }
            });           
            $(targetModal).find('#modalHeader').html('<span>' + title + '</span><button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>');            
        });
        
        
        function returnRefinedURL(key,url)
        {
            var Value  = getQueryVariable(key,url); // This returns kevin
            var stringToBeRemoved = key + '=' + Value; // string becomes 
            var ret = url.replace(stringToBeRemoved+"&", '');
            ret = ret.replace(stringToBeRemoved, '');
            if(ret.substr(ret.length - 1)=='?')
                ret=ret.substr(0,ret.length-1);
            return ret;
        }

        function getQueryVariable(variable,url) {
            var query = url.split("?");
            query= (query.length>1) ? query[1] : '';
            var vars = query.split("&");
            for (var i=0;i<vars.length;i++) {
                var pair = vars[i].split("=");
                if (pair[0] == variable && pair.length>1) {
                    return pair[1];
                }
            } 
            return '';

        }
        
        var pathname = "/restaurant/index2";     
        var url = location.origin+pathname;         
        if(location.pathname==pathname){ 
            url = returnRefinedURL('RestaurantSearch2[restaurant]',location.href);
            url = returnRefinedURL('RestaurantSearch2%5Brestaurant%5D',url);
        }
        $("form.navbar-form").attr('action', url);
        $("form.navbar-form").attr('method', "get");
        $("form.navbar-form input").attr('name', "RestaurantSearch2[restaurant]");
        $("form.navbar-form input").val(getQueryVariable('RestaurantSearch2[restaurant]',location.search) || getQueryVariable('RestaurantSearch2%5Brestaurant%5D',location.search));
        
    });
JS;

$this->registerJs($script);
?>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

