<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = Html::encode(Yii::t('user','Login'));
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jumbotron site-login ">
    <h1><?= $this->title ?></h1>

    <p><?= Html::encode(Yii::t('user','Please fill out the following fields to login:')) ?></p>


    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "<div class=\"col-xs-offset-2 col-xs-8 col-sm-offset-2 col-sm-8 col-md-offset-3 col-md-6 col-lg-offset-4 col-lg-4\">\n{label}\n".
                          "<div class=\"col-xs-12 col-sm-9 col-md-9 col-lg-9\">{input}</div>\n".
                          "<div class=\"text-left col-xs-12 col-sm-offset-3 col-sm-9 col-md-offset-3 col-md-9 col-lg-offset-3 col-lg-9\">{error}</div>\n</div>",
            'labelOptions' => ['class' => 'col-xs-12 text-left-xs col-sm-3 col-md-3 col-lg-3 control-label'],
        ],
    ]); ?>

        <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

        <?= $form->field($model, 'password')->passwordInput() ?>

        <?= $form->field($model, 'rememberMe')->checkbox() ?>

        <div class="form-group">
            <div class="col-md-offset-3 col-md-6 col-lg-offset-4 col-lg-4">
                <?= Html::submitButton(Html::encode(Yii::t('user','Login')), ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                <?= Html::a(Html::encode(Yii::t('user','Register')),'/user/register', ['class' => 'btn btn-info', 'name' => 'login-button']) ?>
            </div>
        </div>

    <?php ActiveForm::end(); ?>

</div>
