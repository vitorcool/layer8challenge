<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = Yii::t('site','Contact');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jumbotron site-contact">
    <h1><?= Html::encode($this->title) ?></h1>

    <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>

        <div class="alert alert-success">
            <?= Html::encode(Yii::t('site','Thank you for contacting us. We will respond to you as soon as possible.')) ?>
        </div>

    <?php else: ?>

        <p>
            <?= Yii::t('site','If you have business inquiries or other questions, please fill out the following form to contact us. <br> Thank you.') ?>
        </p>
        <?php

            $bs_fieldGroup = "col-xs-offset-0 col-xs-12 col-sm-offset-1 col-sm-10 col-md-offset-2 col-md-8 col-lg-offset-2 col-lg-8";
            $bs_label      = "text-left col-xs-12 col-sm-12 col-md-12 col-lg-3";// col-lg-12";
            $bs_input      = "text-left col-xs-12 col-sm-12 col-md-12 col-lg-9";// col-lg-12";
            $bs_error      = "text-left col-xs-12 col-sm-12 col-md-23 col-lg-offset-3 col-lg-9";// col-lg-12";

            $bs_submit     = "col-xs-12 col-sm-12 col-md-12";// col-lg-12";

            $form = ActiveForm::begin([
                'id' => $model->formName(),
                'validationUrl'=>\yii\helpers\Url::toRoute(['site/validate-contact']),
                'enableAjaxValidation' => true,
                'fieldConfig' => [
                    'template' => "<div class=\"".$bs_fieldGroup."\">\n{label}\n".
                        "<div class=\"".$bs_input."\">{input}</div>\n".
                        "<div class=\"".$bs_error."\">{error}</div>\n</div>",
                    'labelOptions' => ['class' => $bs_label.' control-label'],
                    'options' => ['class' => 'form-group row'],
                ]
            ]); ?>
            <?php if(Yii::$app->user->isGuest): ?>
            <?= $form->field($model, 'name')->textInput(['autofocus' => true]) ?>

            <?= $form->field($model, 'email') ?>
            <?php else: ?>
            <?= Html::hiddenInput($model->formName()."[name]",$model->name); ?>
            <?= Html::hiddenInput($model->formName()."[email]",$model->email); ?>
            <?php endif ?>
            <?= $form->field($model, 'subject') ?>

            <?= $form->field($model, 'body')->textarea(['rows' => 6]) ?>

            <?php if(Yii::$app->user->isGuest): ?>
            <?= $form->field($model, 'verify_code')->widget(Captcha::className(), [
               // 'template' => '<div class="row"><div class="col-lg-5">{image}</div><div class="col-lg-7">{input}</div></div>',
            ]) ?>
            <?php endif ?>
            <div class="form-group row">
                <div class="<?= $bs_fieldGroup?>">
                    <div class="<?= $bs_submit?>">
                        <?= Html::submitButton(Yii::t('site','Submit'),
                            ['class' => 'btn btn-primary btn-lg btn-block', 'name' => 'contact-button']) ?>
                    </div>
                </div>
            </div>

        <?php ActiveForm::end(); ?>

    <?php endif; ?>
</div>

<?php
if(Yii::$app->request->isAjax || \app\components\MyHelper::isAjax()) {
    $script = <<<JS
    $('#{$model->formName()}').submit(function(e){
        var isFormAtRootPage=function(form){
            var min=Math.min($(form).attr('action').length,location.href.length);
            return $(form).attr('action').substr(-min)===location.href.substr(-min);
        }
        e.preventDefault();
        
        var form = $(this);
        var data = form.serialize();
        
        if( typeof window.Storage.{$model->formName()}_posting != "undefined" && window.Storage.{$model->formName()}_posting == true) 
            return false;
        window.Storage.{$model->formName()}_posting = true;
        $.post(
            form.attr('action'),
            data
        )
        .done(function (result, textStatus, jqXHR ) {
            console.log("typeof result="+ typeof result);
            if (typeof result=="object" && result.result==true) {
                form.trigger('reset');
                if(! isFormAtRootPage(form) ){
                    if(typeof $.fn.modal == "function" && form.closest('.modal').length==1)
                        form.closest('.modal').modal('hide');
                                        
                    /* update owner page */
                    $(document).trigger("refreshOwnerPage", {type:"{$model->formName()}", "id":"{$model->getId()}" });
                }
            } else if(typeof result=="object" ){                
                $("#message").html(result.message);
            } else if(isFormAtRootPage(form) ){
                $("body").html(result);           
            }
        })
        .fail(function ( jqXHR, textStatus, errorThrown ) {
            console.log("server error");
        }).always(function(){
            window.Storage.{$model->formName()}_posting = false;
        });
        
        
        return false;
    });

JS;
    $this->registerJs($script);
}
?>
