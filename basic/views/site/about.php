<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = Yii::t('site','About');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jumbotron site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::encode(Yii::t('site','This is the About page.')) ?>
    </p>
</div>
