<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
use yii\mongodb\Command;
$this->title = Yii::t('user', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('user', 'Create User'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'class'=> \app\components\EmailColumn::className(),
                'attribute' => 'email'
            ],
            'username',
            [
                'attribute' => 'state',
                'value'     => function ($model, $key, $index, $column) {
                    if(!isset($model->state))
                        $model->state=0;
                    return \app\models\User::getUserStates($model->state);
                },
            ],

            ['class' => \app\components\ActionColumn::className()],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
