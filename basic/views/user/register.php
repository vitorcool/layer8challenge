<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\User */

if(\app\components\MyHelper::isAjax())
    $this->registerAssetBundle( yii\bootstrap\BootstrapAsset::className()  );

$this->title = Yii::t('user', 'Register User');
$this->params['breadcrumbs'][] = ['label' => Yii::t('user', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jumbotron user-register">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_formRegister', [
        'model' => $model,
    ]) ?>

</div>
