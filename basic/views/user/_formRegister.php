<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">
    <?php
    $bs_fieldGroup = "col-xs-offset-0 col-xs-12 col-sm-offset-1 col-sm-10 col-md-offset-2 col-md-8 col-lg-offset-2 col-lg-8";
    $bs_label      = "text-left col-xs-12 col-sm-12 col-md-12 col-lg-3";// col-lg-12";
    $bs_input      = "text-left col-xs-12 col-sm-12 col-md-12 col-lg-9";// col-lg-12";
    $bs_error      = "text-left col-xs-12 col-sm-12 col-md-23 col-lg-offset-3 col-lg-9";// col-lg-12";

    $bs_submit     = "col-xs-12 col-sm-12 col-md-12";// col-lg-12";
    ?>
    <?php $form = ActiveForm::begin([
        'id' => $model->formName(),
        'enableAjaxValidation' => true,
        'validationUrl' => \yii\helpers\Url::toRoute(['user/validate-register-user']),
        'fieldConfig' => [
            'template' => "<div class=\"".$bs_fieldGroup."\">\n{label}\n".
                          "<div class=\"".$bs_input."\">{input}</div>\n".
                          "<div class=\"".$bs_error."\">{error}</div>\n</div>",
            'labelOptions' => ['class' => $bs_label.' control-label'],
            'options' => ['class' => 'form-group row'],
        ],

    ]); ?>

    <?= $form->field($model, 'username') ?>

    <?= $form->field($model, 'email') ?>

    <?= $form->field($model, 'password')->passwordInput() ?>

    <?= $form->field($model, 'verify_code')->widget(\yii\captcha\Captcha::className(), [
      //  'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-9">{input}</div></div>',
    ]) ?>

    <div class="form-group row">
        <div class="<?= $bs_fieldGroup?>">
            <div class="<?= $bs_submit?>">
            <?= Html::submitButton( Yii::t('user', 'Create'),
                                    ['class' => 'btn btn-success btn-lg btn-block']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
if(Yii::$app->request->isAjax) {
    $script = <<<JS
    $('#{$model->formName()}').submit(function(e){
        var isFormAtRootPage=function(form){
            var min=Math.min($(form).attr('action').length,location.href.length);
            return $(form).attr('action').substr(-min)===location.href.substr(-min);
        }
        e.preventDefault();
        
        var form = $(this);
        var data = form.serialize();
        
        if( typeof window.Storage.{$model->formName()}_posting != "undefined" && window.Storage.{$model->formName()}_posting == true) 
            return false;
        window.Storage.{$model->formName()}_posting = true;
        $.post(
            form.attr('action'),
            data
        )
        .done(function (result, textStatus, jqXHR ) {
            if (result==true) {
                form.trigger('reset');
                if(! isFormAtRootPage(form) ){
                    if(typeof $.fn.modal == "function" && form.closest('.modal').length==1)
                        form.closest('.modal').modal('hide');
                                        
                    /* update owner page */
                    // $(document).trigger("refreshOwnerPage", {type:"{$model->formName()}", "id":id, "idx":idx });
                    
                    /*
                    other tecnique is update directly on owner page
                    var elem2replace='.kv-expanded-row[data-key="'+id+'"]';
                    console.log(elem2replace);
                  
                    var url = location.origin + location.pathname + '/grade-details?id=' + id;
                    $(elem2replace).load(url );*/
                    /* end update owner form */
                }
            } else {                
                $("#message").html(result.message);
            }
        })
        .fail(function ( jqXHR, textStatus, errorThrown ) {
            console.log("server error");
        }).always(function(){
            window.Storage.{$model->formName()}_posting = false;
        });
        
        return false;
    });

JS;
    $this->registerJs($script);
}
?>
