<?php

use yii\helpers\Html;
use app\components\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = $model->email;
$this->params['breadcrumbs'][] = ['label' => Yii::t('user', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('user', 'Update'), ['update', 'id' => (string)$model->_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('user', 'Delete'), ['delete', 'id' => (string)$model->_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('user', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            '_id',
            'email',
            'username',
            [
                'attribute' => 'authKey',
                'format'=>'raw',
                'value'=> isset($model->authKey) ? $model->authKey : ""
            ],
            [
                'attribute' => 'accessToken',
                'format'=>'raw',
                'value'=> isset($model->accessToken) ? $model->accessToken : ""
            ],
            [
                'attribute' => 'state',
                'format'=>'raw',
                'value'=> \app\models\User::getUserStates(isset($model->state) ? $model->state:0)
            ],

        ],
    ]) ?>

</div>
