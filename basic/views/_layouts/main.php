<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::t('layout', 'My Company'),
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => Yii::t('layout', 'Home'), 'url' => ['/site/index']],
            ['label' => Yii::t('layout', 'About'), 'url' => ['/site/about']],
            ['label' => Yii::t('layout', 'Contact'), 'url' => ['/site/contact']],
            Yii::$app->user->isGuest ? (
                ['label' => Yii::t('layout', 'Login'), 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    Yii::t('layout', 'Logout').' (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <div class="row">
            <div class="col-lg-6 col-md-8 col-xs-10">
                <span class="float-left bg-info text-white">Theme path</span>
                <span class="float-right bg-primary text-white"><?= $this->theme->getBasePath()?></span>
                <span class="float-left bg-info text-white">Theme url</span>
                <span class="float-right bg-primary text-white"><?= $this->theme->getBaseUrl()?></span>
            </div>
        </div>

        <div class="row">
            <div class="bg-primary text-white">Nullam id dolor id nibh ultricies vehicula ut id elit.</div>
            <div class="bg-success text-white">Duis mollis, est non commodo luctus, nisi erat porttitor ligula.</div>
            <div class="bg-info text-white">Maecenas sed diam eget risus varius blandit sit amet non magna.</div>
            <div class="bg-warning text-white">Etiam porta sem malesuada magna mollis euismod.</div>
            <div class="bg-danger text-white">Donec ullamcorper nulla non metus auctor fringilla.</div>
            <div class="bg-inverse text-white">Cras mattis consectetur purus sit amet fermentum.</div>
            <div class="bg-faded">Cras mattis consectetur purus sit amet fermentum.</div>
        </div>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left"><? Yii::t('layout', '&copy; My Company').' '. date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
