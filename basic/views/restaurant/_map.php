<?php



/* @var $this yii\web\View */
/* @var $model app\models\Restaurant */

$this->title = Yii::t('restaurant', 'Restaurant location Map');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="restaurant-map">
    <?php
    $coord = new \dosamigos\google\maps\LatLng(['lat' => $model->address->coord[1], 'lng' => $model->address->coord[0]]);
    $map = new \dosamigos\google\maps\Map([
        'center' => $coord,
        'zoom' => 14,
    ]);
    $marker = new \dosamigos\google\maps\overlays\Marker([
        'position' => $coord,
        'title' => $model->name,
    ]);
    $map->addOverlay($marker);
    echo $map->display();
    ?>
</div>
<?php
$script= <<<JS
$(window).resize(function(){
    $('div[id*="-map-canvas"]').each(function(idx,elem){
        var w=$(this).parent().width();
        var map=$(this).data("map");
        $(this).css('width',w);
        
        map.setCenter(new google.maps.LatLng({$model->address->coord[1]},{$model->address->coord[0]}));
        google.maps.event.trigger(map, 'resize');
    });
});
$(window).trigger('resize');    
JS;
$this->registerJs($script);


?>