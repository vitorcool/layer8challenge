<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Restaurant */

if(\app\components\MyHelper::isAjax())
    $this->registerAssetBundle( yii\bootstrap\BootstrapAsset::className()  );

$this->title = Yii::t('restaurant', 'Create Restaurant');
$this->params['breadcrumbs'][] = ['label' => Yii::t('restaurant', 'Restaurants'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jumbotron restaurant-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
