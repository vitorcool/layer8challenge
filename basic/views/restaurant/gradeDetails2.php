<?php

use app\components\DetailView;
use yii\bootstrap\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Restaurant */
/* @var $grades app\models\RestaurantGrades */
/* @var $id string */

if(\app\components\MyHelper::isAjax())
    $this->registerAssetBundle( yii\bootstrap\BootstrapAsset::className()  );

?>


<?php
    echo Html::beginTag('div',['class'=>'col-xs-12', ]);
        echo Html::button('+',[
            'class'=>'load-modal btn btn-success circular',
            "data-url"  => \yii\helpers\Url::toRoute(['restaurant/create-grade','rest_id'=>$id], true),
            "data-title"=> Yii::t('restaurant','Create grade about restaurant {name}',['name'=>$model->name]),
        ]);
        echo Html::tag('span',Yii::t('restaurant','Grades'),['class'=>'grades-title']);
    echo Html::endTag('div');
?>
<div class="rest-grades">

    <?php
    foreach($grades as $idx=> $grade):
        echo Html::beginTag('div', [
            'id'        =>"grade" . $idx,
            "class"     => "rest-grade col-xs-12 col-sm-6 col-md-6 col-lg-4",
        ]);
            echo Html::beginTag('div', [
                'class'        =>"load-modal clickable grade-box",
                "data-url"  => \yii\helpers\Url::toRoute(['restaurant/update-grade','rest_id'=>$id,'idx'=>$idx], true),
                "data-title"=> Yii::t('restaurant','Update Grade ({idx}) of {name}',['idx'=>$idx,'name'=>$model->name]),
            ]);
                echo Html::tag('span',Html::encode(
                        app\models\RestaurantGrades::SCORES[$grade->grade].
                        ' - ('.$grade->score.')'
                ),['class'=>'grade-message']);
                echo Html::tag('span',Html::encode(date('Y-m-d H:i:s',$grade->date2)),['class'=>'grade-message pull-right']);
            echo Html::endTag('div');
        echo Html::endTag('div');
    endforeach;
    ?>
</div>
