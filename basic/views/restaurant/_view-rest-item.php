<?php

use app\components\DetailView;
use \yii\helpers\Html;
use \yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Restaurant */


$mainTag = ['class'=>'rest-item col-xs-12 col-sm-12 col-md-12','data-key'=>$model->getId()];
$minItem = ['class'=>'min-item', ];//'style'=>'padding-bottom:20px'];
$fullInfo = ['class'=>'rest-full-info collapse'];
$mapsInfo = ['class'=>'rest-maps-info collapse'];
$commentsInfo = ['class'=>'rest-item-comments collapse'];
$gradesInfo = ['class'=>'rest-item-grades collapse'];

echo Html::beginTag('div',$mainTag);
    echo Html::beginTag('div',$minItem);
        echo Html::tag('span',$model->name);
        echo Html::tag('button','<span class="glyphicon glyphicon-comment"></span>',[
            //'style'=>'float:right',
            'href'=>'#',
            'class'=>'btn btn-info',
            'data-toggle'=>'collapse',
            'title'=>Yii::t('restaurant','Show comments'),
            'data-target'=>'.rest-item[data-key="'.$model->getId().'"] .rest-item-comments'
        ]);
        echo Html::tag('button','<span class="glyphicon glyphicon-grain"></span>',[
            //'style'=>'float:right',
            'href'=>'#',
            'class'=>'btn btn-info',
            'data-toggle'=>'collapse',
            'title'=>Yii::t('restaurant','Show grades'),
            'data-target'=>'.rest-item[data-key="'.$model->getId().'"] .rest-item-grades'
        ]);
        echo Html::tag('button','<span class="glyphicon glyphicon glyphicon-map-marker"></span>',[
            //'style'=>'float:right',
            'href'=>'#',
            'class'=>'load-modal btn btn-info',
            'data-url'=>\yii\helpers\Url::toRoute(['/restaurant/map','id'=>$model->getId()],true),
            'data-title'=>Yii::t('restaurant','Location Map of restaurant "{name}"',['name'=>$model->name]),
           // 'data-target'=>'.rest-item[data-key="'.$model->getId().'"] .rest-maps-info'
        ]);
        echo Html::tag('button','<span class="glyphicon glyphicon-info-sign"></span>',[
            //'style'=>'float:right',
            'href'=>'#',
            'class'=>'btn btn-info',
            'data-toggle'=>'collapse',
            'title'=>Yii::t('restaurant','More Information'),
            'data-target'=>'.rest-item[data-key="'.$model->getId().'"] .rest-full-info'
        ]);

    echo Html::endTag('div');

    echo Html::beginTag('div',$fullInfo);
        echo $this->render('_view-restaurant',['model'=>$model]).
             $this->render('_view-address',['model'=>$model]);
    echo Html::endTag('div');

    echo Html::beginTag('div',$commentsInfo);
        echo $this->render('commentDetails2',['model'=>$model,'comments'=>$model->comments,'id'=>$model->getId()]);
    echo Html::endTag('div');
    echo Html::beginTag('div',$gradesInfo);
        echo $this->render('gradeDetails2',['model'=>$model,'grades'=>$model->grades,'id'=>$model->getId()]);
    echo Html::endTag('div');
echo Html::endTag('div');

?>
