<?php

use app\components\DetailView;
use yii\bootstrap\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Restaurant */
/* @var $grades app\models\RestaurantGrades */
/* @var $form yii\widgets\ActiveForm */
/* @var $id string */

if(\app\components\MyHelper::isAjax())
    $this->registerAssetBundle( yii\bootstrap\BootstrapAsset::className()  );

?>


<?= Html::button('+',[
        'class'=>'load-modal btn btn-success circular',
        "data-url"  => \yii\helpers\Url::toRoute(['restaurant/create-grade','rest_id'=>$id], true),
        "data-title"=> Yii::t('restaurant','Create Grade to restaurante {name}',['name'=>$model->name]),
    ]);
?>
<div class="rest-grades">

    <?php
    foreach($grades as $idx=> $g):
        echo Html::beginTag('div', [
            'id'        =>"grade" . $idx,
            "class"     => "rest-grade col-xs-12 col-sm-6 col-md-6 col-lg-4",
        ]);
        echo Html::beginTag('div', [
            'class'        =>"load-modal clickable grade-box",
            "data-url"  => \yii\helpers\Url::toRoute(['restaurant/update-grade','rest_id'=>$id,'idx'=>$idx], true),
            "data-title"=> Yii::t('restaurant','Update Grade ({idx}) of {name}',['idx'=>$idx,'name'=>$model->name]),
        ]); ?>
            <h4 ><?= $idx ?></h4>
            <p>
                <?php
                echo DetailView::widget([
                    'model' => $model->grades[$idx],
                    'options' => [
                        'data-key'=>$model->id,
                    ],
                    'attributes' => [
                        [
                            'label' => Yii::t('restaurant','Grade'),
                            'value' =>  \app\models\RestaurantGrades::SCORES[$model->grades[$idx]->grade],
                        ],
                        'score',
                        [
                            'label' => Yii::t('restaurant','Date'),
                            'value' => date('Y-M-d', $model->grades[$idx]->date2),
                        ]
                    ],
                ]);
                ?>
            </p>

    <?php
        echo Html::endTag('div');
        echo Html::endTag('div');
    endforeach;
    ?>
</div>
