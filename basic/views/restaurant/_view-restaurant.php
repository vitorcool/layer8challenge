<?php

use app\components\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Restaurant */

echo DetailView::widget([
        'model' => $model,
        'options' => [
            'data-key'=>$model->id,
        ],
        'attributes' => [
            'restaurant_id',
            'name',
            'cuisine',
            'borough',
        ],
    ])
?>
