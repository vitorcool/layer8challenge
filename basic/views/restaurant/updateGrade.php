<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\datecontrol\DateControl;

/* @var $this yii\web\View */
/* @var $model app\models\RestaurantGrades */
/* @var $form ActiveForm */
/* @var $id string */
/* @var $idx integer */


echo $this->render('_formGrade',['model'=>$model,'id'=>$id,'idx'=>$idx]);

?>