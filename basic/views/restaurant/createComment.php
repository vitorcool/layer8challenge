<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\datecontrol\DateControl;

/* @var $this yii\web\View */
/* @var $model app\models\RestaurantComments */
/* @var $form ActiveForm */
/* @var $id string */


echo $this->render('_formComment',['model'=>$model,'id'=>$id]);

?>