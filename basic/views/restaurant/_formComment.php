<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\datecontrol\DateControl;

/* @var $this yii\web\View */
/* @var $model app\models\RestaurantComments */
/* @var $form ActiveForm */
/* @var $id string */
/* @var $idx integer */


if(\app\components\MyHelper::isAjax())
    $this->registerAssetBundle( yii\bootstrap\BootstrapAsset::className()  );
?>

<div class="jumbotron restaurant-Comment">

    <?php
    $bs_fieldGroup = "col-xs-offset-0 col-xs-12 col-sm-offset-1 col-sm-10 col-md-offset-2 col-md-8 col-lg-offset-3 col-lg-6";
    $bs_label      = "text-left col-xs-12 col-sm-12 col-md-3 col-lg-3";// col-lg-12";
    $bs_input      = "text-left col-xs-12 col-sm-12 col-md-9 col-lg-9";// col-lg-12";
    $bs_error      = "text-left col-xs-12 col-sm-12 col-md-offset-3 col-md-9 col-lg-12";// col-lg-12";

    $bs_submit     = "col-xs-12 col-sm-12 col-md-12 col-lg-12";

    $form = ActiveForm::begin([
        'id'=>$model->formName(),
        'options' => ['class' => 'row'],
        'action'=>\yii\helpers\Url::current(['layout'=>null]),
        'validationUrl'=>\yii\helpers\Url::toRoute(['restaurant/validate-comment','rest_id'=>$id]),
        'enableAjaxValidation' => true,
        'fieldConfig' => [
            'template' => "<div class=\"".$bs_fieldGroup."\">\n{label}\n".
                "<div class=\"".$bs_input."\">{input}</div>\n".
                "<div class=\"".$bs_error."\">{error}</div>\n"."</div>",
            'labelOptions' => ['class' => $bs_label.' control-label'],
            'options' => ['class' => 'form-group '],
        ],
    ]); ?>
        <?= Html::hiddenInput('id',$id); ?>
        <?= isset($idx) ? Html::hiddenInput('idx',$idx) : '' ?>
        <?php
        if(Yii::$app->user->isGuest){
            echo $form->field($model, 'name');
            echo $form->field($model, 'email');
        }
        ?>
        <?= $form->field($model, 'message') ?>

        <div class="form-group">
            <div class="<?= $bs_submit?>">
                <div class="<?= $bs_fieldGroup?>">

                    <?= Html::submitButton(!isset($idx) ? Yii::t('restaurant', 'Create') : Yii::t('restaurant', 'Update'),
                        ['class' => !isset($idx) ? 'btn btn-success btn-lg btn-block' : 'btn btn-primary btn-lg btn-block']) ?>
                </div>
            </div>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- restaurant-_formComment -->

<?php
if(Yii::$app->request->isAjax ) {
    $script = <<<JS
    
    $('#{$model->formName()}').submit(function(e){
        var isFormAtRootPage=function(form){
            var min=Math.min($(form).attr('action').length,location.href.length);
            return $(form).attr('action').substr(-min)===location.href.substr(-min);
        }
        e.preventDefault();
        
        var form = $(this);
        var data = form.serialize();
        
        if( typeof window.Storage.{$model->formName()}_posting != "undefined" && window.Storage.{$model->formName()}_posting == true) 
            return false;
        window.Storage.{$model->formName()}_posting = true;
        $.post(
            form.attr('action'),
            data
        )
        .done(function (result, textStatus, jqXHR ) {            
            if (typeof result=="object" && result.result==true) {
                form.trigger('reset');
                if(! isFormAtRootPage(form) ){
                    if(typeof $.fn.modal == "function" && form.closest('.modal').length==1)
                        form.closest('.modal').modal('hide');
                                        
                    /* update owner page */
                    var id= form.find('input[name=id]').val();                   
                    $(document).trigger("refreshOwnerPage", {type:"{$model->formName()}", "id":id});                                                           
                }
            } else {                
                $("#message").html(result.message);
            }
        })
        .fail(function ( jqXHR, textStatus, errorThrown ) {
            console.log("server error");
        }).always(function(){
            window.Storage.{$model->formName()}_posting = false;
        });
        return false;
    });

JS;

    $this->registerJs($script);

}
