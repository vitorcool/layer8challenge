<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;


/* @var $this yii\web\View */
/* @var $searchModel app\models\RestaurantSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('restaurant', 'Restaurants');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="restaurant-index">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('restaurant', 'Create Restaurant'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= kartik\grid\GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'responsive' => true,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            ['class' => '\kartik\grid\ExpandRowColumn',
                'detailRowCssClass' => '',
                'value' => function ($model,$key,$index,$column)
                {
                    return GridView::ROW_COLLAPSED;
                },
                'detailUrl' => \yii\helpers\Url::toRoute(['restaurant/embed-details']),
            ],
            ['class'=>'\kartik\grid\DataColumn',
                //'label'=> Yii::t('restaurant','Restaurant ID'),
                'attribute' => 'restaurant_id',
                'value' => 'restaurant_id'
            ],
            ['class'=>'\kartik\grid\DataColumn',
                //'label'=> Yii::t('restaurant','Name'),
                'attribute' => 'name',
                'value' => 'name'
            ],
            ['class'=>'\kartik\grid\DataColumn',
                //'label'=> Yii::t('restaurant','Cuisine'),
                'attribute' => 'cuisine',
                'value' => 'cuisine'
            ],
            ['class'=>'\kartik\grid\DataColumn',
                //'label'=> Yii::t('restaurant','Borough'),
                'attribute' => 'borough',
                'value' => 'borough'
            ],
            ['class'=>'\kartik\grid\DataColumn',
                //'label'=> Yii::t('restaurant','Building'),
                'attribute' => 'building',
                'value' => 'address.building'
            ],
            ['class'=>'\kartik\grid\DataColumn',
                //'label'=> Yii::t('restaurant','Street'),
                'attribute' => 'street',
                'value' => 'address.street'
            ],
            ['class'=>'\kartik\grid\DataColumn',
                //'label'=> Yii::t('restaurant','Zipcode'),
                'attribute' => 'zipcode',
                'value' => 'address.zipcode'
            ],
            ['class'=>'\kartik\grid\DataColumn',
                //'label'=> Yii::t('restaurant','Coordinates'),
                'attribute' => 'coord',
                'value' => 'address.coord2'
            ],
            ['class' => \app\components\ActionColumn::className()],
        ],
    ]); ?>



<?php Pjax::end(); ?></div>
<?php
$urlGradesUpdate=\yii\helpers\Url::toRoute(['restaurant/grade-details'],true);
$urlCommentsUpdate=\yii\helpers\Url::toRoute(['restaurant/comment-details'],true);
$script = <<< JS
    $(function(){       
        $(document).on('refreshOwnerPage',function(e,info){
            if(info.type=="RestaurantGrades"){
                var elem2replace='#tab-grades-'+info.id;
                console.log(elem2replace);
                var url = '{$urlGradesUpdate}?id=' + info.id;
                $(elem2replace).load(url);
                  
            }else if(info.type=="RestaurantComments"){
                var elem2replace='#tab-comments-'+info.id;
                console.log(elem2replace);
                var url = '{$urlCommentsUpdate}?id=' + info.id;
                $(elem2replace).load(url);                  
            }  
        });
    
    });
JS;

$this->registerJs($script);
?>