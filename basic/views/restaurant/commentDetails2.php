<?php

use app\components\DetailView;
use yii\bootstrap\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Restaurant */
/* @var $comments app\models\RestaurantComment */
/* @var $id string */

if(\app\components\MyHelper::isAjax())
    $this->registerAssetBundle( yii\bootstrap\BootstrapAsset::className()  );

?>


<?php
    echo Html::beginTag('div',['class'=>'col-xs-12', ]);
        echo Html::button('+',[
            'class'=>'load-modal btn btn-success circular',
            "data-url"  => \yii\helpers\Url::toRoute(['restaurant/create-comment','rest_id'=>$id], true),
            "data-title"=> Yii::t('restaurant','Create Comment about restaurant {name}',['name'=>$model->name]),
        ]);
        echo Html::tag('span',Yii::t('restaurant','Comments'),['class'=>'comments-title']);
    echo Html::endTag('div');
?>
<div class="rest-comments">

    <?php
    foreach($comments as $idx=> $comment):
        echo Html::beginTag('div', [
            'id'        =>"comment" . $idx,
            "class"     => "rest-comment col-xs-12 col-sm-6 col-md-6 col-lg-4",
        ]);
            echo Html::beginTag('div', [
                'class'        =>"load-modal clickable comment-box",
                "data-url"  => \yii\helpers\Url::toRoute(['restaurant/update-comment','rest_id'=>$id,'idx'=>$idx], true),
                "data-title"=> Yii::t('restaurant','Update Comment ({idx}) of {name}',['idx'=>$idx,'name'=>$model->name]),
            ]);
                echo Html::tag('span',Html::encode($comment->message),['class'=>'comment-message']);
                echo Html::tag('span',Html::encode(date('Y-m-d H:i:s',$comment->date2)),['class'=>'comment-message pull-right']);
            echo Html::endTag('div');
        echo Html::endTag('div');
    endforeach;
    ?>
</div>
