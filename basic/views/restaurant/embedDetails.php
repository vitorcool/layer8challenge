<?php

use app\components\DetailView;
use yii\bootstrap\Html;
use yii\bootstrap\Tabs;

/* @var $this yii\web\View */
/* @var $model app\models\Restaurant */
/* @var $form yii\widgets\ActiveForm */
/* @var $id string */

if(\app\components\MyHelper::isAjax())
    $this->registerAssetBundle( yii\bootstrap\BootstrapAsset::className()  );

$c1=$this->render('commentDetails',['id'=>$id,'model'=>$model,'comments'=>$model->comments]);
$c2=$this->render('gradeDetails',['id'=>$id,'model'=>$model,'grades'=>$model->grades]);
echo Tabs::widget([
    'items' => [
        [
            'label' => Yii::t('restaurant','Comments'),
         //   'itemOptions'  => ['id'=>'tab-comments-'.$id],
            'options' => ['id'=>'tab-comments-'.$id],
            'content' => $c1,
            'active' => true
        ],
        [
            'label' => Yii::t('restaurant','Grades'),
          //  'itemOptions'  => ['id'=>'tab-grades-'.$id],
            'options' => ['id'=>'tab-grades-'.$id],
            'content' => $c2,

        ],
    ],

]);
?>

