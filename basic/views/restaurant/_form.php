<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Restaurant */
/* @var $form yii\widgets\ActiveForm */

if(\app\components\MyHelper::isAjax())
    $this->registerAssetBundle( yii\bootstrap\BootstrapAsset::className()  );
?>

<div class="restaurant-form">
            <?php
            $bs_fieldGroup = "col-xs-offset-0 col-xs-12 col-sm-offset-1 col-sm-10 col-md-offset-2 col-md-8 col-lg-offset-1 col-lg-5";
            $bs_label      = "text-left col-xs-12 col-sm-12 col-md-3 col-lg-3";// col-lg-12";
            $bs_input      = "text-left col-xs-12 col-sm-12 col-md-9 col-lg-9";// col-lg-12";
            $bs_error      = "text-left col-xs-12 col-sm-12 col-md-offset-3 col-md-9 col-lg-offset-0 col-lg-12";// col-lg-12";


            $bs_submit     = "col-xs-12 col-sm-12 col-md-12";// col-lg-12";
            $bs_buttonGroup= "col-xs-offset-0 col-xs-12 col-sm-offset-1 col-sm-10 col-md-offset-2 col-md-8 col-lg-offset-1 col-lg-11";

            $modelAddress=$model->address;
            $form = ActiveForm::begin([
                'id' => $model->formName(),
                'enableAjaxValidation' => true,
                'validationUrl'=>\yii\helpers\Url::toRoute(['restaurant/validate-restaurant']),
                'fieldConfig' => [
                    'template' => "<div class=\"".$bs_fieldGroup."\">\n{label}\n".
                        "<div class=\"".$bs_input."\">{input}</div>\n".
                        "<div class=\"".$bs_error."\">{error}</div>\n</div>",
                    'labelOptions' => ['class' => $bs_label.' control-label'],
                    'options' => ['class' => 'form-group'],
                ],

            ]); ?>

            <?= $form->field($model, 'restaurant_id') ?>

            <?= $form->field($model, 'name') ?>

            <?= $form->field($model, 'cuisine') ?>

            <?= $form->field($model, 'borough') ?>

            <?= $form->field($modelAddress, 'street') ?>

            <?= $form->field($modelAddress, 'building') ?>

            <?= $form->field($modelAddress, 'zipcode') ?>

            <?= $form->field($modelAddress, 'coord2') ?>

            <div class="form-group">
                <div class="<?= $bs_buttonGroup?>">
                    <div class="<?= $bs_submit?>">
                        <?= Html::submitButton($model->isNewRecord ? Yii::t('restaurant', 'Create') : Yii::t('restaurant', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success btn-lg btn-block' : 'btn btn-primary btn-lg btn-block']) ?>
                    </div>
                </div>
            </div>
            <div class="row"></div>
            <?php ActiveForm::end(); ?>
    </div>
</div>

<?php
if(Yii::$app->request->isAjax) {
    $script = <<<JS
    $('#{$model->formName()}').submit(function(e){
        var isFormAtRootPage=function(form){
            var min=Math.min($(form).attr('action').length,location.href.length);
            return $(form).attr('action').substr(-min)===location.href.substr(-min);
        }
        e.preventDefault();
        
        var form = $(this);
        var data = form.serialize();
        
        if( typeof window.Storage.{$model->formName()}_posting != "undefined" && window.Storage.{$model->formName()}_posting == true) 
            return false;
        window.Storage.{$model->formName()}_posting = true;
        $.post(
            form.attr('action'),
            data
        )
        .done(function (result, textStatus, jqXHR ) {
            if (typeof result=="object" && result.result==true) {
                form.trigger('reset');
                if(! isFormAtRootPage(form) ){
                    if(typeof $.fn.modal == "function" && form.closest('.modal').length==1)
                        form.closest('.modal').modal('hide');
                                        
                    /* update owner page */
                    var id= form.find('input[name=id]').val();
                    $(document).trigger("refreshOwnerPage", {type:"{$model->formName()}", "id":id });
                }
            } else {                
                $("#message").html(result.message);
            }
        })
        .fail(function ( jqXHR, textStatus, errorThrown ) {
            console.log("server error");
        }).always(function(){
            window.Storage.{$model->formName()}_posting = false;
        });
        
        return false;
    });

JS;

    $this->registerJs($script);

}
?>

