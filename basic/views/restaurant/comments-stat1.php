<?php

use yii\helpers\Html;
use miloschuman\highcharts\Highcharts;

/* @var $this yii\web\View */
/* @var $data array */

if(\app\components\MyHelper::isAjax())
    $this->registerAssetBundle( yii\bootstrap\BootstrapAsset::className()  );

$this->title = Yii::t('restaurant', 'Comments Analises');
$this->params['breadcrumbs'][] = ['label' => Yii::t('restaurant', 'Restaurants'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

echo Html::tag('h1',Yii::t('restaurant',''));

$x =array_merge(['x'],$data['year_month']);
echo \app\components\c3\Chart::widget([
    'options' => ['tag' => 'div', 'id'=>'chart_stat2'],
    'clientOptions' => [
        'data'=> [
            'x'=>'x',
            'columns' => [
                $x,
                array_merge(['count'], $data['count']),
                //array_merge(['sum'], $data['sum']),
            ]
        ],
        "axis" => [
            "x" => [

                "type" => 'category',
                "tick" => [
                    "rotate" => 75,
                    "multiline" => false
                ],
                "height" => 130
            ]
        ]
    ]
]);

return;

echo Highcharts::widget([
    'options' => [
        'title' => ['text' => 'Comments Analises'],
        'xAxis' => [
            'categories' => $data['year_month'],
            'title' => ['text' => Yii::t('restaurant','day')],
        ],
        'yAxis' => [
            'title' => ['text' => Yii::t('restaurant','quantity')],
        ],
        'series' => [
            ['name' => Yii::t('restaurant','comments counted'),  'data' => $data['count']],
        ]
    ]
]);

?>
