<?php

use app\components\DetailView;
use yii\bootstrap\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Restaurant */
/* @var $comments app\models\RestaurantComment */
/* @var $id string */

if(\app\components\MyHelper::isAjax())
    $this->registerAssetBundle( yii\bootstrap\BootstrapAsset::className()  );

?>


<?= Html::button('+',[
        'class'=>'load-modal btn btn-success circular',
        "data-url"  => \yii\helpers\Url::toRoute(['restaurant/create-comment','rest_id'=>$id], true),
        "data-title"=> Yii::t('restaurant','Create Comment about restaurant {name}',['name'=>$model->name]),
    ]);
?>
<div class="rest-comments">

    <?php
    foreach($comments as $idx=> $g):
        echo Html::beginTag('div', [
            'id'        =>"comment" . $idx,
            "class"     => "rest-comment col-xs-12 col-sm-6 col-md-6 col-lg-4",
        ]);
        echo Html::beginTag('div', [
            'class'        =>"load-modal clickable comment-box",
            "data-url"  => \yii\helpers\Url::toRoute(['restaurant/update-comment','rest_id'=>$id,'idx'=>$idx], true),
            "data-title"=> Yii::t('restaurant','Update Comment ({idx}) of {name}',['idx'=>$idx,'name'=>$model->name]),
        ]); ?>
            <h4 ><?= $idx ?></h4>
            <p>
                <?php
                echo DetailView::widget([
                    'model' => $model->comments[$idx],
                    'options' => [
                        'data-key'=>$model->id,
                    ],
                    'attributes' => [
                        'message',
                        [
                            'label' => Yii::t('restaurant','User'),
                            'value' => empty($model->comments[$idx]->user)
                                ? $model->comments[$idx]->name.' - '.$model->comments[$idx]->email
                                : $model->comments[$idx]->user->username.' - '.$model->comments[$idx]->user->email
                            ,
                        ],

                        [
                            'label' => Yii::t('restaurant','Date'),
                            'value' => date('Y-M-d', $model->comments[$idx]->date2),
                        ]
                    ],
                ]);
                ?>
            </p>

    <?php
        echo Html::endTag('div');
        echo Html::endTag('div');
    endforeach;
    ?>
</div>
