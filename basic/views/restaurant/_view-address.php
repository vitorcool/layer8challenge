<?php

use app\components\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Restaurant */

echo DetailView::widget([
        'model' => $model->address,
        'options' => [
            'data-key'=>$model->id,
        ],
        'attributes' => [
            'building',
            'street',
            'zipcode',
            'coord2',
        ],
    ])
?>
