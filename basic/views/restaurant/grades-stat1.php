<?php

use yii\helpers\Html;
use miloschuman\highcharts\Highcharts;

/* @var $this yii\web\View */
/* @var $data array */

if(\app\components\MyHelper::isAjax())
    $this->registerAssetBundle( yii\bootstrap\BootstrapAsset::className()  );

$this->title = Yii::t('restaurant', 'Grades Analises');
$this->params['breadcrumbs'][] = ['label' => Yii::t('restaurant', 'Grades Analises'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

echo Html::tag('h1',Yii::t('restaurant','Monthly Grades Analises'));
$x =array_merge(['x'],$data['year_month']);
echo \app\components\c3\Chart::widget([
    'options' => ['tag' => 'div', 'id'=>'chart_stat2'],
    'clientOptions' => [
        'data'=> [
            'x'=>'x',
            'columns' => [
                $x,
                array_merge([Yii::t('restaurant','Total counted grades')], $data['count']),
                array_merge([Yii::t('restaurant','Total summed score')], $data['sum']),
            ]
        ],
        "axis" => [
            "x" => [
                "type" => 'category',
                "tick" => [
                    "rotate" => 75,
                    "multiline" => false
                ],
                "height" => 130
            ]
        ]
    ]
]);

return;
echo Highcharts::widget([
    'options' => [
        'title' => ['text' => Yii::t('restaurant', 'Grades Analises')],
        'xAxis' => [
            'categories' => $data['year_month'],
            'title' => ['text' => Yii::t('restaurant','month')]
        ],
        'yAxis' => [
            'title' => ['text' => Yii::t('restaurant','quantity')]
        ],
        'series' => [
            ['name' => Yii::t('restaurant','grades counted'),  'data' => $data['count']],
            ['name' => Yii::t('restaurant','score sumed'),     'data' => $data['sum']]
        ]
    ]
]);


?>
