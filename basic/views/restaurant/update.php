<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Restaurant */

if(\app\components\MyHelper::isAjax())
    $this->registerAssetBundle( yii\bootstrap\BootstrapAsset::className()  );

$this->title = Yii::t('restaurant', 'Update {modelClass}: ', [
    'modelClass' => 'Restaurant',
]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('restaurant', 'Restaurants'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => (string)$model->_id]];
$this->params['breadcrumbs'][] = Yii::t('restaurant', 'Update');
?>
<div class="jumbotron restaurant-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
