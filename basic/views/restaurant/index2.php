<?php

use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RestaurantSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('restaurant', 'Restaurants Search');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="restaurant-index">
    <h1><?= Html::encode($this->title) ?></h1>
<?php
    $count = $dataProvider->getCount();
    $p=$dataProvider->getPagination();
    $totalCount = $dataProvider->getTotalCount();
    $begin = $p->getPage() * $p->pageSize + 1;
    $end = $begin + $count - 1;
    if ($begin > $end) {
        $begin = $end;
    }
?>
<div class="summary"><?= Yii::t('restaurant','Showing <b>{begin}-{end}</b> of <b>{totalCount}</b> items.',
        [
            'begin'=>$begin,
            'end'=>$end,
            'totalCount'=>$totalCount
        ]) ?></div>
<?php Pjax::begin(); ?>
    <div id="rest-search" class="row">
    <?php
    foreach ($dataProvider->getModels() as $model) {
       echo $this->render('_view-rest-item',['model'=>$model]);
    }
    ?>
    </div>
    <?php
    echo \yii\widgets\LinkPager::widget([
        'pagination'=>$dataProvider->pagination,
    ]);
?>


<?php Pjax::end(); ?></div>
<?php
//$urlGradesUpdate=\yii\helpers\Url::toRoute(['restaurant/grade-details2'],true);
$urlCommentsUpdate=\yii\helpers\Url::toRoute(['restaurant/comment-details2'],true);
$urlGradesUpdate=\yii\helpers\Url::toRoute(['restaurant/grade-details2'],true);
$script = <<< JS
$(function(){       
        $(document).on('refreshOwnerPage',function(e,info){
            if(info.type=="RestaurantComments"){
                var elem2replace='tr.kv-expand-detail-row[data-key="'+info.id+'"] div.kv-expanded-row';
                elem2replace = '.rest-item[data-key="'+info.id+'"] .rest-item-comments';
                console.log(elem2replace);
                var url = '{$urlCommentsUpdate}?id=' + info.id;
                $(elem2replace).load(url);                  
            }  
            if(info.type=="RestaurantGrades"){
                var elem2replace='tr.kv-expand-detail-row[data-key="'+info.id+'"] div.kv-expanded-row';
                elem2replace = '.rest-item[data-key="'+info.id+'"] .rest-item-grades';
                console.log(elem2replace);
                var url = '{$urlGradesUpdate}?id=' + info.id;
                $(elem2replace).load(url);                  
            }  
        });
    
    });
JS;

$this->registerJs($script);
?>