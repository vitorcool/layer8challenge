<?php

use yii\helpers\Html;
use miloschuman\highcharts\Highcharts;

/* @var $this yii\web\View */
/* @var $data array */

if(\app\components\MyHelper::isAjax())
    $this->registerAssetBundle( yii\bootstrap\BootstrapAsset::className()  );

$this->title = Yii::t('restaurant', 'Comments Analises');
$this->params['breadcrumbs'][] = ['label' => Yii::t('restaurant', 'Comments Analises'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$barData = \yii\helpers\Json::encode( ['json' => $data , 'type'=>'bar']   );
$barXLabel = Yii::t('restaurant','Comments stats');
$barYLabel = Yii::t('restaurant','time (seconds)');
echo Html::tag('h1',Yii::t('restaurant','Statistic time between comments'));
echo \app\components\c3\Chart::widget([
    'options' => ['tag' => 'div', 'id'=>'chart_stat2'],
    'clientOptions' => [
        "data" => [
            'json' => $data,
             'type'=> "bar",
                        ["Max.",$data['max']],
                        ["Avg.",$data['avg']],
                        ["Min.",$data['min']]
            ],
        "type" => 'bar',
        "axis" =>[
            "rotated" => true,
            "labels" => [
                'y' => $barYLabel
            ],
        ]
    ],
]);


$script= <<<JS
    setTimeout(function () {
        chart_stat2.axis.labels({         
            y: '{$barYLabel}',          
        });
    }, 1000);

JS;
$this->registerJs($script);


return;
echo Highcharts::widget([
    'options' => [
        "responsive" => [
            "rules" => [[
                "condition" => [
                    "maxWidth" => 200,
                    "maxHeight" => 200
                    ],
                "chartOptions" => [
                    "legend" => [
                        "layout" => 'horizontal',
                        "align"  => "center",
                        "verticalAlign" => "bottom"
                    ]
                ]
            ]]
        ],
        "chart" => [ "type"=>"bar"],
        "title" => [Yii::t('restaurant','Intervalo entre mensagens')],
      //  "subtitle" => ['text' => 'Source: <a href="#">link</a>'],

        "xAxis" => [
            "categories" => null,
            "title" => [ "text" => Yii::t('restaurant',"Comments") ],
        ],
        "yAxis" => [
            "min" => 0,
                "title" => [
                    "text" => [Yii::t('restaurant','Time (seconds)')],
                    "align"=> "high",
                 ],
                "labels" => [
                    "overflow" => 'justify'
                ]
            ],
        "tooltip" => [
            "valueSuffix" => " seconds"
        ],
        "plotOptions" => [
            "bar" => [
                "minPointLength" => 1,
                "dataLabels" => [
                    "enabled" => true
                    ]
            ]
        ],

/*        "legend" => [
            "layout" => 'vertical',
            "align" => 'right',
            "verticalAlign" => 'top',
            "x" => -40,
            "y" => 80,
            "floating" => true,
            "borderWidth" => 1,
            "backgroundColor" => "((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF')",
            "shadow" => true
        ],*/
        "credits" => [
            "enabled" => false
        ],
        "series" => [
            [
                "name" => Yii::t('restaurant',"Max"),
                "data" => [$data['max']]
            ],
            [
                "name" => Yii::t('restaurant',"Avg"),
                "data" => [$data['avg']]
            ],
            [
                "name" => Yii::t('restaurant',"Min"),
                "data" =>  [$data['min']]
            ]
        ]
    ]
]);

?>
